/**
 * 
 */
package it.unipi.di.marcoSpinosa.progettoReti;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @author marco
 *
 */
public class RCLAddress {
	
	/**
	 * Variabili di istanza
	 */
	private final String ip;
	private final int port;
	private final boolean isProxyAddress;
	
	
	/**
	 * Stringhe costanti per passare da java a json e vice-versa
	 */
	private static final String IP 				 = "ip";
	private static final String PORT 			 = "port";
	private static final String IS_PROXY_ADDRESS = "isProxyAddress";
	
	/**
	 * -------------------------------------------------------------- COSTRUTTORI ----------------------------------------------------------------------
	 */
	
	/**
	 * @param ip
	 * @param port
	 * @param isProxyAddress 
	 */
	public RCLAddress ( final String ip, final int port, final boolean isProxyAddress  ) {
		super();
		this.ip 			= ip;
		this.port 			= port;
		this.isProxyAddress = isProxyAddress;
	}
	
	/**
	 * @param address
	 */
	public RCLAddress ( final JSONObject  address){
		super();
		this.ip 			= (String) address.get(IP);
		this.port 			= ((Number) address.get(PORT)).intValue();
		this.isProxyAddress = ((Boolean) address.get(IS_PROXY_ADDRESS));
	}
	
	/**
	 * ------------------------------------------------------------------METODI-------------------------------------------------------------------------
	 */
	
	/**
	 * @return p
	 */
	@SuppressWarnings("unchecked")
	public JSONObject toJSON(){
		
		JSONObject address = new JSONObject();
		
		address.put(IP, this.ip);
		address.put(PORT, this.port);
		address.put(IS_PROXY_ADDRESS, this.isProxyAddress);

		return address;
	}
	
	/**
	 * -------------------------------------------------------------METODI STATICI ---------------------------------------------------------------------
	 */
	
	/**
	 * @param address
	 * @return p
	 * @throws ParseException 
	 */
	public static RCLAddress getAddressFromString(String address) throws ParseException {
	
		JSONObject newAddressJSON = (JSONObject) new JSONParser().parse( address );
	
		return new RCLAddress( newAddressJSON );
	}
	
	public static String getAddress (String defaultAddress, Integer port, Boolean isProxyAddress) {

		if ( RCLConstant.DEBUG ){
			
			System.out.println( "------------------------------------------\n" );
			
			System.out.print( "GET ADDRESS: " );
		}
		
		Enumeration<NetworkInterface> nets = null;

		try {

			nets = NetworkInterface.getNetworkInterfaces();

		} catch ( SocketException e ) {
			
			if (RCLConstant.DEBUG) {
				
				System.out.println( "Problema con le interfaccie di rete" );
				
				System.out.println( "restituisco l'indirizzo di Default [" + defaultAddress +"]" );
				
				System.out.println( "------------------------------------------\n" );
			}

			return defaultAddress;	
		}

		for ( NetworkInterface netint : Collections.list( nets ) ) {

			String interfaceName = netint.getDisplayName();

			if ( interfaceName.equals( RCLConstant.VBOXNET0 ) || interfaceName.startsWith( RCLConstant.ETH ) ) {

				Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();

				for ( InetAddress inetAddress : Collections.list( inetAddresses ) ) {

					String[] aStrings = inetAddress.getHostAddress().split("\\." );

					if ( aStrings[0].equals( "172" ) ) {
						
						if (RCLConstant.DEBUG) {
							
							System.out.println( "Interfaccia desiderata trovata" );
							
							System.out.println( "restituisco l'indirizzo [" + inetAddress.getHostAddress() +"]" );
							
							System.out.println( "------------------------------------------\n" );
						}
						
						return new RCLAddress( inetAddress.getHostAddress(), port, isProxyAddress ).toJSON().toJSONString();
					}
				}
			}
		}
		
		if (RCLConstant.DEBUG) {
			
			System.out.println( "Interfaccia desiderata non trovata" );
			
			System.out.println( "restituisco l'indirizzo di Default [" + defaultAddress +"]" );
			
			System.out.println( "------------------------------------------\n" );
		}
		
		return new RCLAddress( defaultAddress, port, isProxyAddress ).toJSON().toJSONString();
	}
	
	/**
	 * -------------------------------------------------------------METODI GETTER ----------------------------------------------------------------------
	 */
	
	/**
	 * @return the ip
	 */
	public String getIP() {
		return ip;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @return the isProxyAddress
	 */
	public boolean isProxyAddress() {
		return isProxyAddress;
	}

}

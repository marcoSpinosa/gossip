/**
 *  Package per il progetto finale del Laboratorio di programmazione di rete
 */
package it.unipi.di.marcoSpinosa.progettoReti;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


/**
 * @author marco
 *
 */
public class RCLMsgDataStruct {

	/**
	 * Variabili di istanza
	 */
	private final String mittente;
	private final String destinatario;
	private final String messaggio;
	
	/**
	 * Stringhe costanti per passare da java a json e vice-versa
	 */
	private static final String MITTENTE 	 = "mittente";
	private static final String DESTINATARIO = "destinatario";
	private static final String MESSAGGIO 	 = "messaggio";

	/**
	 * -------------------------------------------------------------- COSTRUTTORI ----------------------------------------------------------------------
	 */
	
	/**
	 * Costruttore
	 * Crea un oggetto di tipo messaggio
	 * @param mittente Il mittente del messaggio
	 * @param destinatario Il destinatario del messaggio
	 * @param messaggio Il testo del messaggio
	 */
	public RCLMsgDataStruct(String mittente, String destinatario, String messaggio) {
		super();
		this.mittente 		= mittente;
		this.destinatario 	= destinatario;
		this.messaggio 		= messaggio;
	}
	
	/**
	 * Ricostruisce un messaggio (oggetto java)
	 * a partire da un oggetto JSON
	 * @param msg il messaggio JSON da cui ricavare quello java
	 */
	public RCLMsgDataStruct (final JSONObject msg) {
		super();
		this.mittente 		= (String) msg.get(MITTENTE);
		this.destinatario 	= (String) msg.get(DESTINATARIO);
		this.messaggio 		= (String) msg.get(MESSAGGIO);
	}
	
	/**
	 * ------------------------------------------------------------------METODI-------------------------------------------------------------------------
	 */
	
	
	/**
	 * Crea e restituisce una compia del messaggio in formato JSON
	 * @return msg Stesso messaggio ma in formato JSON
	 */
	@SuppressWarnings("unchecked")
	public JSONObject toJSON() {
		
		JSONObject msg = new JSONObject();
		
		msg.put(MITTENTE, this.mittente);
		msg.put(DESTINATARIO, this.destinatario);
		msg.put(MESSAGGIO, this.messaggio);
		
		return msg;
	}
	
	/**
	 * -------------------------------------------------------------METODI STATICI ---------------------------------------------------------------------
	 */
	
	/**
	 * Crea e restituisce un messaggio partendo da una stringa
	 * @allert La stringa passata deve essere una stringa JSON che rappresenta un msg
	 * @param msg la stringa che rappresenta il messaggio
	 * @return IL messaggio (oggetto java) estratto dalla stringa
	 * @throws ParseException se la stringa passata non è una stringa JSON che rappresenta un messaggio
	 */
	public static RCLMsgDataStruct getMsgFromString(String msg) throws ParseException {
	
		JSONObject newMsgJSON = (JSONObject) new JSONParser().parse( msg );
	
		return new RCLMsgDataStruct( newMsgJSON );
	}
	
	
	/**
	 * -------------------------------------------------------------METODI GETTER ----------------------------------------------------------------------
	 */
	
	/**
	 * @return Il mittente del messaggio
	 */
	public String getMittente() {
		return this.mittente;
	}

	/**
	 * @return Il destinatario del messaggio
	 */
	public String getDestinatario() {
		return this.destinatario;
	}

	/**
	 * @return Il testo del messaggio
	 */
	public String getMessaggio() {
		return this.messaggio;
	}
	/**
	 * ---------------------------------------------------------------------------------------------------------------------------------------------------
	 */
}

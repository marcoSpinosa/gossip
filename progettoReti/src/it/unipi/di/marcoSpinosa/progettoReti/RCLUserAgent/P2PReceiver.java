/**
 *  Package per il progetto finale del Laboratorio di programmazione di rete
 */
package it.unipi.di.marcoSpinosa.progettoReti.RCLUserAgent;

import it.unipi.di.marcoSpinosa.progettoReti.RCLConstant;
import it.unipi.di.marcoSpinosa.progettoReti.RCLMsgDataStruct;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;

import org.json.simple.parser.ParseException;


/**
 * @author marco
 *
 */
public class P2PReceiver extends Thread {

	    private DatagramSocket socket = null;
	    private int port;
	    private RCLUserAgent userAgent;

	    /**
	     * @param name
	     * @param port 
	     * @throws IOException
	     */
	    public P2PReceiver(final String name, final int port) throws IOException {
	        super(name);
	        this.setDaemon(true);
	        this.socket = new DatagramSocket(port);
	        this.userAgent = null;
	    }
	    
	    /**
	     * @param name
	     * @throws IOException
	     */
	    public P2PReceiver(final String name) throws IOException {
	        super(name);
	        this.setDaemon(true);
	        this.socket = new DatagramSocket();
	        this.setPort( this.socket.getLocalPort() );
	        this.userAgent = null;
	    }

	    /* (non-Javadoc)
	     * @see java.lang.Thread#run()
	     */
	    @SuppressWarnings("javadoc")
		public void run() {
	        
	    	byte[] buf = new byte[256];
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
	    	
	    	while (true) {
	            
	    		try {
	                socket.receive(packet);
	                
	                RCLMsgDataStruct msg = RCLMsgDataStruct.getMsgFromString( new String(packet.getData(), 0, packet.getLength()));
	                ArrayList<String> list = this.userAgent.getUser().getListaIngresso();
	                
	                if (! list.contains( msg.getMittente() )) {
	                	System.out.println("ATTENZIONE: il mittente di questo messaggio non è nella tua lista in ingresso!");
	                }
	                System.out.println(msg.getMittente() + " ha scritto:\n" + msg.getMessaggio());
	                
	            } catch (IOException e) {
	            	System.out.println("ATTENZIONE: sono stati riscontrati problemi di connessione\n da ora non sarà più possibile ricevere messaggi...si consiglia il riavvio del programma");
	                this.socket.close();
	                break;
	                
	            } catch ( ParseException e ) {
					if (RCLConstant.DEBUG) System.out.println("P2PReceiver receive from: " + packet.getSocketAddress() + "\n" + new String(packet.getData(), 0, packet.getLength ()));
	            	continue;
				}
	        }
	    
	    }

		/**
		 * @return the port
		 */
		public int getPort() {
			return port;
		}

		/**
		 * @param port the port to set
		 */
		public void setPort ( int port ) {
			this.port = port;
		}

		/**
		 * @return the userAgent
		 */
		public RCLUserAgent getUserAgent () {
			return userAgent;
		}

		/**
		 * @param userAgent the userAgent to set
		 */
		public void setUserAgent ( RCLUserAgent userAgent ) {
			this.userAgent = userAgent;
		}
}

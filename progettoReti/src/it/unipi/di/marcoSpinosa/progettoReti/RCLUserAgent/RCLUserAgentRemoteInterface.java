/**
 *  Package contenenti le classi per la realizzazione del componente UA del servizio GOSSIP
 */
package it.unipi.di.marcoSpinosa.progettoReti.RCLUserAgent;

import it.unipi.di.marcoSpinosa.progettoReti.RCLAddress;

import java.rmi.*;


/**
 * 
 * Interfaccia remota utilizzata dalla Registry per inviare notifiche agli UA
 * 
 * @author Marco Spinosa 477385
 * 
 */
public interface RCLUserAgentRemoteInterface extends Remote {
	
	/**
	 * Funzione che permette alla Registry di notificare che nickname
	 * è online e che address è il suo nuovo indirizzo
	 * 
	 * @param nickname dell'utente che si è appena connesso al servizio
	 * @param address dell'utente che si è appena connesso al servizio
	 * 
	 * @throws RemoteException se non è possibile comunicare con lo UA
	 */
	public void notifyUserOnline(String nickname, String address) throws RemoteException;
	
	/**
	 * Funzione che permette alla Registry di notificare che nickname è offline
	 * 
	 * @param nickname dell'utente che si è appena disconnesso dal servizio
	 * 
	 * @throws RemoteException se non è possibile comunicare con lo UA
	 */
	public void notifyUserOffline(String nickname) throws RemoteException;
	
	/**
	 * Funzione che permette alla Registry di notificare che nickname ha inviato una
	 * richiesta di amicizia all'utente loggato da questo UA
	 * 
	 * @param nickname dell'utente che ha effettuato la richiesta
	 * 
	 * @throws RemoteException se non è possibile comunicare con lo UA
	 */
	public void notifyFriendFrom(String nickname) throws RemoteException;
	
	/**
	 * Funzione che permette alla Registry di notificare che nickname ha eliminato
	 * dagli amici l'utente loggato da questo UA
	 * 
	 * @param nickname dell'utente che ha effettuato la richiesta
	 * 
	 * @throws RemoteException se non è possibile comunicare con lo UA
	 */
	public void notifyUnfriendFrom(String nickname) throws RemoteException;
	
	/**
	 * Funzione che permette alla Registry di notificare che nickname ha accettato
	 * la richiesta di amicizia inviatogli dall'utente loggato su questo UA
	 * 
	 * @param nickname dell'utente che ha effettuato la richiesta
	 * 
	 * @throws RemoteException se non è possibile comunicare con lo UA
	 */
	public void notifyAllowFrom(String nickname) throws RemoteException;
	
	/**
	 * Funzione che permette alla Registry di notificare che nickname ha rimosso
	 * dai contatti bloccati l'utente loggato su questo UA
	 * 
	 * @param nickname dell'utente che ha effettuato la richiesta
	 * 
	 * @throws RemoteException se non è possibile comunicare con lo UA
	 */
	public void notifyReallowFrom(String nickname) throws RemoteException;
	
	/**
	 * Funzione che permette alla Registry di notificare che nickname ha 
	 * bloccato l'utente loggato su questo UA
	 * 
	 * @param nickname dell'utente che ha effettuato la richiesta
	 * 
	 * @throws RemoteException se non è possibile comunicare con lo UA
	 */
	public void notifyDisallowFrom(String nickname) throws RemoteException;
	
	/**
	 * Funzione che permette alla Registry di chiedere allo UA il suo indirizzo
	 * 
	 * @return {@link RCLAddress RCLAddress}.toString che rappresenta l'indirizzo dello UA
	 * 
	 * @throws RemoteException se non è possibile comunicare con lo UA
	 */
	public String getAddress () throws RemoteException;
	
	/**
	 * Funzione che permette alla Registry di verificare che lo UA sia "vivo"
	 * (una specie di Ping)
	 * 
	 * @throws RemoteException se non è possibile comunicare con lo UA
	 */
	public void isAlive () throws RemoteException;

}

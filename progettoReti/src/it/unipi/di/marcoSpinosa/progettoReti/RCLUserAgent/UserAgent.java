/**
 *  Package per il progetto finale del Laboratorio di programmazione di rete
 */
package it.unipi.di.marcoSpinosa.progettoReti.RCLUserAgent;

import it.unipi.di.marcoSpinosa.progettoReti.RCLConstant;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NoSuchObjectException;
import java.rmi.server.UnicastRemoteObject;
import java.util.regex.PatternSyntaxException;

/**
 * @author marco
 *
 */
public class UserAgent {
	
	private static int port;
	private static BufferedReader input = null;
	private static RCLUserAgent userAgent = null;
		
	/**
	 * @param args
	 */
	public static void main(final String[] args) {
		
		try{
			
			switch ( args.length ) {
				
				case 0:
						port = -1;
						break;
						
				case 1:
						try {
								port = Integer.parseInt(args[0]);
							
						} catch ( NumberFormatException ex ) {
							//Non è stato passato un numero come primo argomento
							System.out.println("ATTENZIONE!:\nNumero di porta non corretto, ne verrà utilizzata una libera.");
							port = -1;
						}
						break;
					
				default:
					System.out.println("ATTENZIONE!:\nTroppi argomenti passati");
					System.out.println("USAGE:\njava -jar UserAgent <port>");
					System.exit(-1);
					break;
			}
			
			P2PReceiver daemon = null;
			
			try {																	//Avvio Daemon UDP
				
				if ( port == -1 ) {
					
					daemon = new P2PReceiver("UDPDaemon");							//Avvio il Daemon su una porta libera
					port = daemon.getPort();
					if (RCLConstant.DEBUG) System.out.println("Daemon collegato alla porta " + port);
					
				} else {
					daemon = new P2PReceiver("UDPDaemon", port);
				}
				
				daemon.start();
				
			} catch ( IllegalThreadStateException ex ) {
				//Daemon gi?? in esecuzione...Per come ?? strutturato il programma non dovrebbe mai essere lanciata
				System.out.println("Daemon UDP gi?? in esecuzione");
				System.out.println("ERRORE RISCONTRATO: " + ex.getLocalizedMessage());
				System.exit(-1);
				
			} catch ( IOException ex ) {
				//Di solito porta gi?? in uso...ma anche eccezioni dovute a DatagramSocket e setDaemon
				System.out.println("ATTENZIONE!:\nIl daemon UDP non pu?? essere avviato.\nNon sessendo in grado di garantire il servizio l'applicazione verr?? chiusa.");
				System.out.println("ERRORE RISCONTRATO: " + ex.getLocalizedMessage());
				System.exit(-1);
			}
			
			
			final String EMPTY = "";
			
			String in, command, nickname, user = RCLConstant.NO_USER;
			
			command = in = nickname = EMPTY;
			
			input = new BufferedReader(new InputStreamReader(System.in));
			
			userAgent = new RCLUserAgent(port);
			
			System.setProperty("java.rmi.server.hostname", userAgent.getAddress());
			
			daemon.setUserAgent( userAgent );
			
			String array[] = null;
		
			while ( true ) {
					
					System.out.print(user + "-> ");
					in = input.readLine().toLowerCase();
					
					try{		
							
						array = in.split(" ");																														
							
					} catch (PatternSyntaxException ex ){
						//Regex non valida ...Per come ?? strutturato il programma non dovrebbe essere mai lanciata
						System.out.println("ATTENZIONE!:\nPattern di riconoscimento comando non valido");
						System.exit(-1);
					}
					
					switch (array.length) {
																																					//CASE 0: se si preme invio viene restituita la stringa vuota -> array.length = 1;
						case 1:																														//Tutti i comandi tranne HELP necessitato di un argomento
								if ( !(RCLConstant.HELP_COMMAND.equals( array[0] ) ||
										RCLConstant.NICK_INFO_COMMAND_UA.equals( array[0] ) ||
										RCLConstant.FRIEND_REQUEST.equals( array[0] ))) {
									
									RCLConstant.printError(array[0]);
									
									continue;
								}
								command = array[0];
								break;
								
						case 2:																														//COMANDO + NICKNAME [presumibilmente]
								command = array[0];
								nickname = array[1];
								break;
							
						default:																														//Qualsiasi altro numero di argomento ?? sbagliato
								System.out.println("ATTENZIONE!:\nTroppi parametri passati");
								System.out.println("Usa " + RCLConstant.HELP_COMMAND + " per visualizzare le istruzioni\n");
								continue;
					}
					
					switch( command ) {
					
						case RCLConstant.REGISTER_COMMAND:
							
								switch( userAgent.register( nickname ) ){
								
									case RCLConstant.NO_PROXY:
											System.out.println("ATTENZIONE!:\nAl momento non sono disponibili Proxy, l'utente " + nickname + " non ?? stato registrato.");
											System.out.println("Non essendo in grado di fornire il servizio l'applicazione verr?? chiusa, ci scusiamo per il disagio.\n");
											System.exit(-1);
											break;
										
									case RCLConstant.USER_ALREADY_REGISTERED:
											System.out.println("ATTENZIONE!:\nUtente gi?? registrato");
											System.out.print("Utilizza " + RCLConstant.LOGIN_COMMAND + " " + nickname + " per effettuare la connessione." );
											System.out.println("oppure registrati con " + RCLConstant.REGISTER_COMMAND + " nuovoNickname." );
											break;
											
									case RCLConstant.REGISTRATION_OK:
											System.out.println("L'utente " + nickname + " ?? stato registrato con successo");
											break;
											
									default:
											System.out.println("ATTENZIONE!:\nESITO OPERAZIONE INDESIDERATO");
											System.exit(-1);
											break;
								}
								break;
						
						case RCLConstant.LOGIN_COMMAND:
							
								switch( userAgent.login(nickname) ){
								
									case RCLConstant.USER_ALREADY_LOGGED:
											System.out.println("ATTENZIONE!:\nUn solo utente alla volta pu?? essere loggato.");
											System.out.println("Prima di effettuare il nuovo login disconnettere " + user );
											break;
											
									case RCLConstant.USER_LOGGED_FROM_DIFFERENT_UA:
											System.out.println("ATTENZIONE!:\nQuesto utente ?? gi?? loggato su un altro userAgent!");
											System.out.println("Si pu?? effettuare il login su un solo userAgent alla volta.");
											break;
										
									case RCLConstant.UNREGISTERED_USER:
											System.out.println( "Per utilizzare il servizio bisogna prima registrarsi." );
											System.out.println( "Utilizza " + RCLConstant.REGISTER_COMMAND + " " + nickname + " per effettuare la registrazione." );
											break;
									
									case RCLConstant.LOGIN_OK:
											System.out.println("L'utente " + nickname + " ?? ora connesso al servizio");
											user = nickname;
											break;
											
									default:
											System.out.println("ATTENZIONE!:\nESITO OPERAZIONE INDESIDERATO");
											System.exit(-1);
											break;
								}
								break;
							
								
						case RCLConstant.LOGOUT_COMMAND:
							
								switch( userAgent.logout(nickname) ){
								
									case RCLConstant.NO_LOGGED_USER:
											System.out.println("Il comando " + RCLConstant.LOGOUT_COMMAND + " non pu?? essere usato se prima non si ?? effettuato un login.");
											System.out.println("Usa " + RCLConstant.HELP_COMMAND + " per visualizzare le istruzioni\n");
											break;
									
									case RCLConstant.WRONG_NICKNAME:
											System.out.println("Non puoi effettuare il logout di un altro utente!");
											break;
										
									case RCLConstant.LOGOUT_OK:
											System.out.println("L'utente " + nickname + " si ?? disconnesso dal servizio.");
											user = RCLConstant.NO_USER;
											break;
											
									default:
											System.out.println("ATTENZIONE!:\nESITO OPERAZIONE INDESIDERATO");
											System.exit(-1);
											break;
								}
								break;
							
								
						case RCLConstant.SEND_COMMAND:
								
								in = input.readLine();
								
								switch( userAgent.sendMessageTo(nickname, in) ){
									
									case RCLConstant.NO_LOGGED_USER:
											System.out.println("Il comando " + RCLConstant.SEND_COMMAND + " pu?? essere utilizzato solo dopo aver effettuato il login.");
											break;
											
									case RCLConstant.FRIEND_REQUEST_FROM_NICKNAME:
											System.out.println("Non puoi inviare messaggii a "+nickname + " finch?? non accetti la sua richiesta di amicizia.");
											System.out.println("Usa " + RCLConstant.ALLOW_COMMAND + " " + nickname + " per accettarla");
											break;
											
									case RCLConstant.BLOCKED_BY_NICKNAME:
											System.out.println("Sei stato bloccato da " + nickname + " !\nFinch?? non ti sblocca non potrete comunicare");
											break;
											
									case RCLConstant.BLOCKED_NICKNAME:
											System.out.println("Hai bloccato " + nickname + " !\nPrima di comunicare devi sbloccarlo.");
											System.out.println("Usa " + RCLConstant.ALLOW_COMMAND + " " + nickname + " per farlo");
											break;
											
									case RCLConstant.SEND_NO:
											System.out.println(nickname + " non ?? un tuo amico!\nPuoi comunicare solo con i tuoi amici.");
											System.out.println("Usa " + RCLConstant.FRIEND_COMMAND + " " + nickname + " per inviarli una richiesta di amicizia");
											break;
											
									case RCLConstant.WAITING_FOR_ALLOW:
											System.out.println(nickname + " non ha ancora accettato la tua richiesta di amicizia.");
											System.out.println("Dovrai attendere conferma prima di poter comunicare");
											break;
											
									case RCLConstant.SEND_OK:
											System.out.println("Inviato");
											break;
											
									default:
											System.out.println("ATTENZIONE!:\nESITO OPERAZIONE INDESIDERATO");
											System.exit(-1);
											break;
								}
								break;
							
								
						case RCLConstant.ALLOW_COMMAND:
								
								switch( userAgent.allow(nickname) ){
											
//									case RCLConstant.UNREGISTERED_USER:
//											System.out.println(nickname + " non ?? un utente registrato al servizio;");
//											System.out.println("Puoi confermare la richiesta di amicizia solo ad altri utenti del servizio");
//											break;
								
									case RCLConstant.NO_LOGGED_USER:
											System.out.println("Il comando " + RCLConstant.ALLOW_COMMAND + " pu?? essere utilizzato solo dopo aver effettuato il login.");
											break;
									
									case RCLConstant.ALLOW_BEFORE_FRIEND:
											System.out.println("Non puoi accettare la richiesta di amicizia di " + nickname + " se ancora non te l'ha inviata!");
											break;
											
									case RCLConstant.ALLOW_FRIEND:
											System.out.println("Hai gi?? accettato la richiesta di amicizia di "+ nickname);
											break;
											
									case RCLConstant.ALLOW_OK:
											System.out.println("Hai accettato la richiesta di amiciazia di " + nickname);
											break;
								
									case RCLConstant.REALLOW_OK:
											System.out.println("Hai sbloccato " + nickname);
											break;
											
									default:
											System.out.println("ATTENZIONE!:\nESITO OPERAZIONE INDESIDERATO");
											System.exit(-1);
											break;
										
								}
								break;
							
						case RCLConstant.DISALLOW_COMMAND:
								
								switch(userAgent.disallow( nickname )){
									
									case RCLConstant.NO_LOGGED_USER:
											System.out.println("Il comando " + RCLConstant.DISALLOW_COMMAND + " pu?? essere utilizzato solo dopo aver effettuato il login.");
											break;
									
//									case RCLConstant.UNREGISTERED_USER:
//											System.out.println(nickname + " non ?? un utente registrato al servizio;");
//											System.out.println("Puoi bloccare solo i tuoi amici");
//											break;
										
									case RCLConstant.DISALLOWED_USER:
											System.out.println("Hai gi?? bloccato l'utente " + nickname + ".");
											break;
											
									case RCLConstant.DISALLOW_NO:
											System.out.println(nickname + " non ?? un tuo amico;");
											System.out.println("Puoi bloccare solo i tuoi amici.");
											break;
										
									case RCLConstant.DISALLOW_OK:
											System.out.println("Hai bloccato " + nickname);
											break;
										
									default:
											System.out.println("ATTENZIONE!:\nESITO OPERAZIONE INDESIDERATO");
											System.exit(-1);
											break;
								}
								break;
							
								
						case RCLConstant.FRIEND_COMMAND:
								
								switch(userAgent.friend( nickname )){
								
									case RCLConstant.NO_LOGGED_USER:
											System.out.println("Il comando " + RCLConstant.FRIEND_COMMAND + " pu?? essere utilizzato solo dopo aver effettuato il login.");
											break;
											
									case RCLConstant.ACTION_AGAINST_YOURSELF:
											System.out.println("Non puoi inviarti una richiesta di amicizia da sola.");
											break;
											
									case RCLConstant.UNREGISTERED_USER:
											System.out.println(nickname + " non ?? un utente registrato al servizio;");
											System.out.println("Puoi richiedere l'amicizia solo ad altri utenti del servizio");
											break;
											
									case RCLConstant.USER_ALREADY_LOGGED:
											System.out.println(nickname + " ?? gi?? connesso al servizio;");
											break;
											
									case RCLConstant.FRIEND_REQUEST_FROM_NICKNAME:
											System.out.println("Non Puoi inviare una richiesta di amicizia a "+nickname + ", lo ha gi?? fatto lui.");
											System.out.println("Usa " + RCLConstant.ALLOW_COMMAND + " " + nickname + " per accettarla");
											break;
									
									case RCLConstant.FRIEND_REQUEST_SENDED:
											System.out.println("Hai gi?? inviato una richiesta di amicizia a " + nickname);
											break;
											
									case RCLConstant.BLOCKED_NICKNAME:
											System.out.println("Tu e  " + nickname + " siete gi?? amici, usa " + RCLConstant.ALLOW_COMMAND + " per sbloccarlo e ricomiciare a comunicare.");
											break;
										
									case RCLConstant.BLOCKED_BY_NICKNAME:
											System.out.println("Tu e " + nickname + " siete gi?? amici, ma lui ti ha bloccato.\nDevi aspettare che ti sblocchi per poter comunicare.");
											break;
											
									case RCLConstant.FRIEND_OK:
											System.out.println("Hai inviato con successo una richiesta di amicizia a " + nickname);
											break;
											
									case RCLConstant.FRIEND_NO:
											System.out.println("Tu e " + nickname + " siete gi?? amici!");
											break;
										
									default:
											System.out.println("ATTENZIONE!:\nESITO OPERAZIONE INDESIDERATO");
											System.exit(-1);
											break;
								}
								break;
							
						case RCLConstant.UNFRIEND_COMMAND:
							
								switch(userAgent.unfriend( nickname )){
							
									case RCLConstant.NO_LOGGED_USER:
											System.out.println("Il comando " + RCLConstant.FRIEND_COMMAND + " può essere utilizzato solo dopo aver effettuato il login.");
											break;
													
									case RCLConstant.DENY_FRIEND_OK:
											System.out.println("Hai rifiutato la richiesta di amicizia di " + nickname + ".");
											break;
										
									case RCLConstant.FRIEND_REQUEST_CANCEL:
											System.out.println("Hai cancellato la richiesta di amicizia inviata a " + nickname + ".");
											break;
										
									case RCLConstant.UNFRIEND_NO:
											System.out.println(nickname + " non è un tuo amico.");
											System.out.println("Puoi utilizzare questo comando per rimuovere qualcuno dai tuoi amici e/o per rifiutare le richieste di amicizia indesiderate.");
											break;
											
									case RCLConstant.UNFRIEND_OK:
											System.out.println("Hai rimosso " + nickname + " dagli amici.");
											break;
										
									default:
											System.out.println("ATTENZIONE!:\nESITO OPERAZIONE INDESIDERATO");
											System.exit(-1);
											break;
								}
								break;
						
								
						case RCLConstant.NICK_INFO_COMMAND_UA:
								
								if (RCLConstant.DEBUG) {
									
									userAgent.getInfoForUA();
								}
								else {
									System.out.println("Comando non valido\nUsa " + RCLConstant.HELP_COMMAND + " per visualizzare le istruzioni\n");
								}
								break;
								
						case RCLConstant.FRIEND_REQUEST:

								userAgent.getFriendRequest();

							break;
							
								
						case RCLConstant.HELP_COMMAND:
								if (RCLConstant.DEBUG) {
									
									RCLConstant.printHelpForDebug();
								} else {
									RCLConstant.printHelp();
								}
								break;
						
						default:
								System.out.println("Comando non valido\nUsa " + RCLConstant.HELP_COMMAND + " per visualizzare le istruzioni\n");
								break;
					}
			}

		} catch (Exception ex) {
			System.err.println( "UserAgent exception: " + ex.getLocalizedMessage( ) ); 
			ex.printStackTrace();
			System.exit(-1);
		}
		
		finally{
			
			System.out.println("Fine");
			
			try{
			
				input.close();
				System.in.close();
				UnicastRemoteObject.unexportObject(userAgent, false);
				
			} catch ( NoSuchObjectException ex ){
				//Oggetto attualmente non esportato...Per come ?? strutturato il programma non dvrebbe essere mai lanciata
				System.out.println("ATTENZIONE\nImpossibile disconnettere lo user-agent.\nuser-agent non trovato.");
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		}

}

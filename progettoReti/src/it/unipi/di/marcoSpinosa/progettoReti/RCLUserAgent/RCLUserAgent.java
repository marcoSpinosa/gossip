/**
 * Package per il progetto finale del Laboratorio di programmazione di rete
 */
package it.unipi.di.marcoSpinosa.progettoReti.RCLUserAgent;

import it.unipi.di.marcoSpinosa.progettoReti.RCLAddress;
import it.unipi.di.marcoSpinosa.progettoReti.RCLConstant;
import it.unipi.di.marcoSpinosa.progettoReti.RCLMsgDataStruct;
import it.unipi.di.marcoSpinosa.progettoReti.RCLUserDataStruct;
import it.unipi.di.marcoSpinosa.progettoReti.RCLProxy.RCLProxyDataStruct;
import it.unipi.di.marcoSpinosa.progettoReti.RCLRegistry.RCLRegistryRemoteInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @author marco Classe che implementa lo userAgent del progetto
 */
public class RCLUserAgent extends UnicastRemoteObject implements RCLUserAgentRemoteInterface {

	private static final long							serialVersionUID	= 1L;
	
	
	private boolean										userLogged 	= false;
	
	private RCLUserDataStruct							user		= null;

	private String										userNickname= null;
	
	
	private RCLRegistryRemoteInterface					server 		= null;
	
	private RCLProxyDataStruct							proxy		= null;

	private DatagramSocket								UDPSocket	= null;

	private int											port		= 0;
	
	private String 										address 	= null;
	

	private static HashMap<String, RCLProxyDataStruct>	offline		= new HashMap<String, RCLProxyDataStruct>(RCLConstant.INITIAL_NUMBER_OF_USER );
	
	private static HashMap<String, RCLAddress>			online		= new HashMap<String, RCLAddress>(RCLConstant.INITIAL_NUMBER_OF_USER );
	
	private static ArrayList<RCLProxyDataStruct>		proxyArray	= new ArrayList<RCLProxyDataStruct>(RCLConstant.NUMBER_OF_PRESUMED_INITIAL_PROXY );


	public RCLUserAgent ( int port ) throws RemoteException {
		
		super();

		setUserLogged( false );

		try {

			server = (RCLRegistryRemoteInterface) Naming.lookup( RCLConstant.GOSSIP );
			
			UDPSocket = new DatagramSocket();

		} catch ( NotBoundException ex ) {
			// Nome Servizio Sbagiato o non avviato
			System.out.println( "ATTENZIONE!:\nIl servizio richiesto potrebbe non essere disponibile.\nRiprovare in un altro momento.\n" );
			
			System.exit( -1 );

		} catch ( RemoteException ex ) {
			// Di solito Registry non avviato
			System.out.println( "ATTENZIONE!:\nImpossibile contattare il server.\nCi scusiamo per il disagio.\n" );
			
			System.exit( -1 );

		} catch ( MalformedURLException ex ) {
			// Indirizzo passato non ?? un URL...Per come ?? strutturato il
			// programma non dovrebbe essere mai lanciata
			System.out.println( "ATTENZIONE!:\nURL del server non corretta! \n" );
			
			System.exit( -1 );

		} catch ( SocketException e ) {
			System.out.println( "ATTENZIONE!:\nImpossibile creare socket UDP.\nNon potendo garantire il sevizio l'applicazione verr?? chiusa.\n" );
			
			System.exit( -1 );
		}

		this.port = port;
	}

	/**
	 * Funzione che esegue la registrazione di nickname al servizio Gossip
	 * 
	 * @param nickname dell'utente da registrare
	 * 
	 * @return NO_PROXY Se al momento non ci sono proxy disponibili
	 *         <p>
	 *         USER_ALREADY_REGISTER se l'utente ?? gi?? registrato
	 *         <p>
	 *         REGISTRATION_OK se la registrazione ?? avvenuta con successo
	 * 
	 * @throws RemoteException Se non ?? possibile contattare il server
	 */
	public String register ( String nickname ) throws RemoteException {
		
		return server.register( nickname );
	}

	/**
	 * Funzione che connette nickname al servizio Gossip
	 * 
	 * @param nickname dell'utente da loggare 
	 * 
	 * @return 
	 * 			{@link RCLConstant#USER_LOGGED_FROM_DIFFERENT_UA USER_LOGGED_FROM_DIFFERENT_UA}
	 * 			se l'utente è già connsesso da un altro userAgent
	 * 		
	 * 		<p>	{@link RCLConstant#USER_ALREADY_LOGGED USER_ALREADY_LOGGED}
	 * 			se un utente è già loggato su questo UA
	 * 		
	 * 		<p>	{@link RCLConstant#UNREGISTERED_USER UNREGISTERED_USER} se l'utente non è registrato
	 * 		
	 * 		<p>	{@link RCLUserDataStruct#toJSON Stringa contentente le informazioni dell'utente} 
	 * 			se l'utente è stato connesso al servizio
	 * 
	 *@throws RemoteException Se non è possibile contattare il server
	 */
	public synchronized String login ( String nickname ) throws RemoteException {

		if ( isUserLogged() ) {
			
			return RCLConstant.USER_ALREADY_LOGGED;
		}

		String result = server.login( nickname, this );

		if ( RCLConstant.UNREGISTERED_USER.equals( result ) ) {
			
			return RCLConstant.UNREGISTERED_USER;
		}

		if ( RCLConstant.USER_LOGGED_FROM_DIFFERENT_UA.equals( result ) ) {
			
			return RCLConstant.USER_LOGGED_FROM_DIFFERENT_UA;
		}

		try {

			setUser( RCLUserDataStruct.getUserDataFromString( result ) );

		} catch ( ParseException e ) {
			System.out.println( "ERORRE FATALE:\nImpossibile registrare le informazioni dell'utente, le informazioni ricevute non sono parsabili" );
			System.exit( -1 );
		}

		try {

			proxy = RCLProxyDataStruct.getProxyFromString( user.getProxyInfo() );

		} catch ( ParseException e ) {
			
			System.out.println( "ERORRE FATALE:\nImpossibile registrare il proxy, le informazioni ricevute non sono parsabili" );
			
			System.exit( -1 );
		}

		downloadFromProxy( nickname );

		setUserLogged( true );

		userNickname = nickname;

		return RCLConstant.LOGIN_OK;
	}

	private void downloadFromProxy ( String nickname ) {

		Socket proxySocket = null;
		
		PrintWriter toProxy = null;
		
		BufferedReader fromProxy = null;
		
		try {

			proxySocket = new Socket( proxy.getIP(), proxy.getPort() );
			
			toProxy = new PrintWriter( proxySocket.getOutputStream(), true );
			
			fromProxy = new BufferedReader( new InputStreamReader( proxySocket.getInputStream() ) );

			toProxy.println( (new RCLMsgDataStruct( nickname, RCLConstant.TO_PROXY, RCLConstant.PROXY_GET_MSG )).toJSON().toJSONString() );
			
			String ricevuto = fromProxy.readLine();

			JSONObject newMsgJSON = (JSONObject) new JSONParser().parse( ricevuto );

			@SuppressWarnings ( "unchecked" )
			List<String> list = (List<String>) newMsgJSON.get( RCLConstant.PROXY_MSG_LIST );
			
			Iterator<String> iterator = list.iterator();

			if ( iterator.hasNext() ) {

				System.out.println( "MESSAGGI RICEVUTI MENTRE ERI OFFLINE\n------------------------------------\n" );

				while ( iterator.hasNext() ) {

					RCLMsgDataStruct msg = RCLMsgDataStruct.getMsgFromString( iterator.next() );
					
					System.out.println( msg.getMittente() + " ti ha inviato:\n" + msg.getMessaggio() + "\n" );
				}

				System.out.println( "\n------------------------------------\n" );
			}

		} catch ( UnknownHostException e ) {

			System.err.println( "ATTENZIONE!:\nImpossibile trovare un proxy con questo indirizzo." );
			System.exit( -1 );

		} catch ( IOException e ) {

			System.err.println( "ATTENZIONE!:\nImpossibile connettersi con il proprio proxy" );
			System.exit( -1 );

		} catch ( ParseException e ) {
			System.err.println( "ATTENZIONE!:\nImpossibile decifrare i messaggi ricevuti dal proxy" );
			System.exit( -1 );

		} finally {

			try {
				proxySocket.close();
				
			} catch ( IOException e ) {
				
				System.err.println( "Problema chiusura socketProxy" );
			}
			proxySocket = null;
		}
	}

	/**
	 * Funzione che disconnette nickname dal servizio Gossip
	 * 
	 * @param nickname dell'utente da disconnettere
	 * 
	 * @return 
	 * 			{@link RCLConstant#NO_LOGGED_USER NO_LOGGED_USER}
	 * 			se l' utente non è connesso al servizio
	 * 		
	 * 		<p>	{@link RCLConstant#WRONG_NICKNAME WRONG_NICKNAME}
	 * 			se nickname non corrisponde all'utente attualmente loggato sullo UA

	 * 		<p>	{@link RCLConstant#LOGOUT_OK LOGOUT_OK} se l'utente è stato disconnesso dal servizio
	 * 
	 *@throws RemoteException Se non è possibile contattare il server
	 */
	public synchronized String logout ( String nickname ) throws RemoteException {

		if ( !isUserLogged() ) {
			
			return RCLConstant.NO_LOGGED_USER;
		}

		if ( !nickname.equals( userNickname ) ) {
			
			return RCLConstant.WRONG_NICKNAME;
		}

		setUserLogged( false );
		
		userNickname = RCLConstant.NO_USER;
		
		online = new HashMap<String, RCLAddress>(RCLConstant.INITIAL_NUMBER_OF_USER );
		
		return server.logout( nickname );
	}

	/**
	 * Funzione che permette lo scambio di messaggi tra due utenti del servizio
	 * Gossip Se l'utente a cui inviare il messaggio ?? online si utilizza il
	 * protocollo UDP per realizzare una comunicazione P2P; Se l'utente a cui
	 * inviare il messaggio ?? offline si utilizza il protocollo TCP per
	 * comunicare con il proxy a lui assegnato
	 * 
	 * @param nickname dell'utente a cui inviare il messaggio
	 * @param msg messaggio da inviare a nickname
	 * 
	 * @return NO_LOGGED_USER se nessun utente ?? connesso
	 *         <p>
	 *         BLOCKED_FROM_NICKNAME se l'utente ?? stato bloccato da nickname
	 *         <p>
	 *         BLOCKED_NICKNAME se l'utente ha bloccato nickname
	 *         <p>
	 *         FRIEND_REQUEST_FROM_NICKNAME l'utente e nickname non sono ancora amici ma lui gli ha inviato una richiesta di amicizia
	 *         <p>
	 *         WAITING_FOR_ALLOW se l'utente ha inviato una richesta di amicizia a nickname ma questa non ?? stata ancora accettata
	 *         <p>
	 *         SEND_NO se l'utente e nickname non sono amici
	 *         <p>
	 *         SEND_OK se il messaggio ?? stato inviato
	 */
	public synchronized String sendMessageTo ( final String nickname, String msg ) {

		if ( !isUserLogged() ) {
			return RCLConstant.NO_LOGGED_USER;
		}
		
		if ( user.getBloccatoDa().contains( nickname ) ) {
			return RCLConstant.BLOCKED_BY_NICKNAME;
		}

		if ( user.getListaContattiBloccati().contains( nickname ) ) {
			return RCLConstant.BLOCKED_NICKNAME;
		}

		if ( user.getListaRichiesteDiAmicizia().contains( nickname ) ) {
			return RCLConstant.FRIEND_REQUEST_FROM_NICKNAME;
		}

		if ( !user.getListaIngresso().contains( nickname ) ) {

			if ( !user.getListaUscita().contains( nickname ) ) {

				return RCLConstant.SEND_NO;
			}

			return RCLConstant.WAITING_FOR_ALLOW;
		}

		RCLMsgDataStruct messaggio = new RCLMsgDataStruct( userNickname, nickname, msg );

		if ( online.containsKey( nickname ) ) {

			RCLAddress address = online.get( nickname );
			
			return sendOverUDP( messaggio, address );
		}

		if ( offline.containsKey( nickname ) ) {

			RCLProxyDataStruct proxy = offline.get( nickname );
			
			return sendOverTCP( messaggio, proxy );
		}

		RCLAddress address = null;

		try {
			
			address = RCLAddress.getAddressFromString( server.getAddressByNickString( nickname ) );

		} catch ( RemoteException | ParseException e ) {
			
			e.printStackTrace();
			
			return RCLConstant.SEND_NO;// TODO AGGIUSTARE MESSAGGIO
		}

		if ( address.isProxyAddress() ) {
			
			addProxy(address, nickname);
						
			return sendOverTCP( messaggio, new RCLProxyDataStruct(address) );

		} else {
			
			sendOverUDP( messaggio, address );
			
			online.put( nickname, address );
		}
		return RCLConstant.SEND_OK;
	}

	/**
	 * @param msg
	 * @param address
	 */
	private String sendOverUDP ( final RCLMsgDataStruct msg, final RCLAddress address ) {
		
		byte[] buffer = new byte[256];
		
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length );
		
		buffer = msg.toJSON().toJSONString().getBytes();
		
		packet.setData( buffer );
		
		packet.setLength( buffer.length );
		
		packet.setPort( address.getPort() );

		try {

			packet.setAddress( InetAddress.getByName( address.getIP() ) );
			
			UDPSocket.send( packet );

		} catch ( UnknownHostException e ) {
			
			System.err.println( "ATTENZIONE!:\nImpossibile trovare uno UA con questo indirizzo." );
			
			return RCLConstant.SEND_NO;
		
		} catch ( IOException e ) {
			// TODO Auto-generated catch block

			System.err.println( "ATTENZIONE!:\nImpossibile connettersi con lo UA desiderato." );
			
			return RCLConstant.SEND_NO;
		}
		
		return RCLConstant.SEND_OK;
	}
	
	private String sendOverTCP ( final RCLMsgDataStruct messaggio, final RCLProxyDataStruct proxy ) {
	
		Socket proxySocket = null;

		PrintWriter toProxy = null;
		
		try {

			proxySocket = new Socket( proxy.getIP(), proxy.getPort() );

			toProxy = new PrintWriter( proxySocket.getOutputStream(), true );

			toProxy.println( messaggio.toJSON().toJSONString() );

		} catch ( UnknownHostException e ) {

			System.err.println( "ATTENZIONE!:\nImpossibile trovare un proxy con questo indirizzo." );
			
			return RCLConstant.SEND_NO;

		} catch ( IOException e ) {

			System.err.println( "ATTENZIONE!:\nImpossibile connettersi con questo proxy" );
			
			return RCLConstant.SEND_NO;

		} finally {

			toProxy = null;

			try {
				
				proxySocket.close();

			} catch ( final IOException e ) {
				
				System.err.println( "Problema chiusura proxySocket" );
			}
			
			proxySocket = null;
		}
		
		return RCLConstant.SEND_OK;
	}

	/**
	 * Funzione che permette ad un utente di accettare una richiesta di amicizia
	 * o di sbloccare un utente precedentemente bloccato con una "Disallow"
	 * 
	 * @param nickname dell'utente di cui accettare la richiesta di amicizia/da sbloccare
	 * 
	 * @return NO_LOGGED_USER se nessun utente ?? connesso
	 *         <p>
	 *         ALLOW_FRIEND se nickname ?? gi?? un amico dell'utente loggato
	 *         <p>
	 *         ALLOW_BEFORE_FRIEND se si prova a accettare una richiesta di amicizia non esistente
	 *         <p>
	 *         ALLOW_OK se l'utente ha accettato con successo la richiesta di amicizia di nickname
	 *         <p>
	 *         REALLOW_OK se l'utente ha sbloccato con successo nickname
	 * 
	 * @throws RemoteException Se non ?? possibile contattare il server
	 */
	public synchronized String allow ( String nickname ) throws RemoteException {

		if ( !isUserLogged() ) {
			
			return RCLConstant.NO_LOGGED_USER;
		}

		if ( user.getListaRichiesteDiAmicizia().contains( nickname ) ) { 
			
			user.allowNewFriend( nickname );
			
			user.addNewFriend( nickname );
			
			return server.allow( userNickname, nickname );
		}

		if ( user.getListaContattiBloccati().contains( nickname ) ) {
			
			user.reallowFriend( nickname );
			
			return server.reallow( userNickname, nickname );
		}

		if ( user.getListaUscita().contains( nickname ) ) {
			
			if ( user.getListaIngresso().contains( nickname ) ) {
				
				return RCLConstant.ALLOW_FRIEND;
			}
		}

		return RCLConstant.ALLOW_BEFORE_FRIEND;
		// Se il controllo fosse stato fatto sul server ci sarebbe stata
		// la distinzione con UNREGISTERED_USER
	}

	/**
	 * Funzione che permette ad un utente di bloccarne un altro
	 * 
	 * @param nickname dell'utente da bloccare
	 * 
	 * @return NO_LOGGED_USER se nessun utente ?? connesso
	 *         <p>
	 *         DISALLOW_NO se nickname ?? gi?? stato bloccato dall'utente loggato
	 *         <p>
	 *         DISALLOW_OK se nickname ?? stato bloccato con successo
	 * 
	 * @throws RemoteException Se non ?? possibile contattare il server
	 */
	public synchronized String disallow ( String nickname ) throws RemoteException {

		if ( !isUserLogged() ) {
			
			return RCLConstant.NO_LOGGED_USER;
		}

		if ( user.getListaContattiBloccati().contains( nickname ) ) {
			
			return RCLConstant.DISALLOWED_USER;
		}

		if ( user.getListaIngresso().contains( nickname ) ) {
			
			user.blockFriend( nickname );
			
			return server.disallow( userNickname, nickname );
		}

		return RCLConstant.DISALLOW_NO;
	}

	/**
	 * @param nickname
	 * @return s
	 * @throws RemoteException Se non ?? possibile contattare il server
	 */
	public synchronized String friend ( String nickname ) throws RemoteException {

		if ( !isUserLogged() ) {
			
			return RCLConstant.NO_LOGGED_USER;
		}
		
		if ( userNickname.equals( nickname ) ) {
			
			return RCLConstant.ACTION_AGAINST_YOURSELF;
		}

		if ( user.getListaRichiesteDiAmicizia().contains( nickname ) ) {
			
			return RCLConstant.FRIEND_REQUEST_FROM_NICKNAME;
		}

		if ( user.getListaUscita().contains( nickname ) ) {

			if ( user.getListaContattiBloccati().contains( nickname ) ) {
				
				return RCLConstant.BLOCKED_NICKNAME;
			}

			if ( user.getBloccatoDa().contains( nickname ) ) {
				
				return RCLConstant.BLOCKED_BY_NICKNAME;
			}

			if ( user.getListaIngresso().contains( nickname ) ) {
				
				return RCLConstant.FRIEND_NO;
			}

			return RCLConstant.FRIEND_REQUEST_SENDED;
		}

		String result = server.friend( userNickname, nickname );

		if ( RCLConstant.FRIEND_OK.equals( result ) ) {
			
			user.addNewFriend( nickname );
		}
		return result;
	}

	/**
	 * @param nickname
	 * @return s
	 * @throws RemoteException Se non è possibile contattare il server
	 */
	public synchronized String unfriend ( String nickname ) throws RemoteException {

		if ( !isUserLogged() ) {
			
			return RCLConstant.NO_LOGGED_USER;
		}

		if ( user.getListaRichiesteDiAmicizia().contains( nickname ) ) {

			server.unfriend( userNickname, nickname );
			
			user.removeFriend( nickname );
			
			return RCLConstant.DENY_FRIEND_OK;
		}

		if ( user.getListaUscita().contains( nickname ) ) {

			if ( user.getListaIngresso().contains( nickname ) ) {
				
				user.removeFriend( nickname );
				
				return server.unfriend( userNickname, nickname );

			}
			
			user.removeFriend( nickname );
			
			server.unfriend( userNickname, nickname );
			
			return RCLConstant.FRIEND_REQUEST_CANCEL;

		}
		
		return RCLConstant.UNFRIEND_NO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.unipi.di.marcoSpinosa.progettoReti.RCLUserAgentIF#notifyUserOnline
	 * (java.lang.String, java.lang.String, java.lang.String)
	 */
	@SuppressWarnings ( "javadoc" )
	@Override
	public synchronized void notifyUserOnline ( final String nickname, final String address ) throws RemoteException {

		offline.remove( nickname );

		try {

			online.put( nickname, RCLAddress.getAddressFromString( address ) );

		} catch ( ParseException e ) {
			
			System.out.print( "\nGOSSIP: ParseError\n" + userNickname + "->" );
			
			return;
		}

		System.out.print( "\nGOSSIP: L'utente " + nickname + " è passato online\n" + userNickname + "->" );

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.unipi.di.marcoSpinosa.progettoReti.RCLUserAgentIF#notifyUserOffline
	 * (java.lang.String)
	 */
	@SuppressWarnings ( "javadoc" )
	@Override
	public synchronized void notifyUserOffline ( final String nickname ) throws RemoteException {
		
		online.remove( nickname );
		
		System.out.print( "\nGOSSIP: L'utente " + nickname + " è passato offline\n" + userNickname + " ->" );

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.unipi.di.marcoSpinosa.progettoReti.RCLUserAgentRemoteInterface#
	 * notifyFriendFrom(java.lang.String)
	 */
	@SuppressWarnings ( "javadoc" )
	@Override
	public synchronized void notifyFriendFrom ( final String nickname ) throws RemoteException {
		
		System.out.println( "\nGOSSIP: Hai ricevuto una richiesta di amicizia da " + nickname );
		
		System.out.print( "Usa " + RCLConstant.ALLOW_COMMAND + " " + nickname + " per accettarla\n" + userNickname + "->" );
		
		user.addFriendRequest( nickname );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.unipi.di.marcoSpinosa.progettoReti.RCLUserAgentRemoteInterface#
	 * notifyAllowFrom(java.lang.String)
	 */
	@SuppressWarnings ( "javadoc" )
	@Override
	public synchronized void notifyAllowFrom ( final String nickname ) throws RemoteException {
		
		System.out.print( "\nGOSSIP: " + nickname + " ha accettato la tua richiesta di amicizia\n" + userNickname + "->" );
		
		user.allowNewFriend( nickname );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.unipi.di.marcoSpinosa.progettoReti.RCLUserAgentRemoteInterface#
	 * notifyReallowFrom(java.lang.String)
	 */
	@SuppressWarnings ( "javadoc" )
	@Override
	public synchronized void notifyReallowFrom ( final String nickname ) throws RemoteException {
		
		System.out.print( "\nGOSSIP: " + nickname + " ti ha sbloccato!\nOra potete ricominciare a comunicare\n" + userNickname + "->" );
		
		user.unblockFrom( nickname );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.unipi.di.marcoSpinosa.progettoReti.RCLUserAgentRemoteInterface#
	 * notifyDisallowFrom(java.lang.String)
	 */
	@SuppressWarnings ( "javadoc" )
	@Override
	public synchronized void notifyDisallowFrom ( final String nickname ) throws RemoteException {
		
		System.out.print( "\nGOSSIP: " + nickname + " ti ha bloccato!\nFinchè non ti sbloccherrà non potrete comunicare\n" + userNickname + "->" );
		
		user.blockFrom( nickname );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.unipi.di.marcoSpinosa.progettoReti.RCLUserAgentRemoteInterface#
	 * notifyUnfriendFrom(java.lang.String)
	 */
	@SuppressWarnings ( "javadoc" )
	@Override
	public synchronized void notifyUnfriendFrom ( final String nickname ) throws RemoteException {

		if ( user.getListaRichiesteDiAmicizia().contains( nickname ) ) {
			
			System.out.print( "\nGOSSIP: "+ nickname + " ha cancellato la richiesta di amicizia che ti aveva inviato.\n"+ userNickname + "->" );
		
		} else {
			
			System.out.print( "\nGOSSIP: " + nickname + " ti ha eliminato da i suoi amici.\n" + userNickname + "->" );
		}

		user.removeFriend( nickname );
	}



	/**
	 * @return the userLogged
	 */
	public boolean isUserLogged () {
		
		return userLogged;
	}

	/**
	 * @param bool
	 *            Booleano che indica se un utente ?? connesso
	 */
	public void setUserLogged ( boolean bool ) {
		
		this.userLogged = bool;
	}

	/**
	 * @return the user
	 */
	public RCLUserDataStruct getUser () {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser ( RCLUserDataStruct user ) {
		
		this.user = user;
	}

	/**
	 * TODO da rimuovere
	 */
	public void stampaUserDetail () {
		
		if ( isUserLogged() ) {
			
			user.stampaUserDetail();
		}
	}

	/**
	 * @return the port
	 */
	public int getPort () {
		
		return port;
	}

	private void addProxy ( RCLAddress proxyAddress, String nickname ) {
		
		if (RCLConstant.DEBUG) {
			
			System.out.println( "\n------------------------------------------" );
			
			System.out.println( "ADD PROXY..." );
		}

		Iterator<RCLProxyDataStruct> iteratore = proxyArray.iterator();
		boolean trovato = false;
		RCLProxyDataStruct p = null;

		while ( iteratore.hasNext() && !trovato ) {

			p = (RCLProxyDataStruct) iteratore.next();

			if ( p.getIP().equals( proxyAddress.getIP() ) && (p.getPort() == proxyAddress.getPort()) ) {
				
				offline.put( nickname, p );
				
				trovato = true;
				
				System.out.println( "Proxy Già Presente:\nIP:" + p.getIP() + "\nPORT:" + p.getPort() );
			}
		}

		if ( !trovato ) {

			RCLProxyDataStruct newProxy = new RCLProxyDataStruct( proxyAddress );
			
			proxyArray.add( newProxy );
			
			offline.put( nickname, newProxy );
			
			System.out.println( "Nuovo Proxy Aggiunto:\nIP:" + newProxy.getIP() + "\nPORT:" + newProxy.getPort() );
		}
		
		if (RCLConstant.DEBUG) {
			
			System.out.println( "------------------------------------------\n" );
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.unipi.di.marcoSpinosa.progettoReti.RCLUserAgentRemoteInterface#isAlive
	 * ()
	 */
	@SuppressWarnings ( "javadoc" )
	@Override
	public void isAlive () throws RemoteException {

	}

	/**
	 */
	public synchronized void getInfoForUA() {
		
		System.out.println("\n------------------------------------------");
		
		if ( isUserLogged() ){
			
			System.out.println("INFO " + userNickname.toUpperCase() + " :\n");
			
			user.stampaUserDetail();
		}
		else {
			
			System.out.print("\nNessun utente loggato");
		}
		
		System.out.println("\n------------------------------------------");
	}
	
	public synchronized void getFriendRequest() {
		
		System.out.println("\n------------------------------------------");
		
		if ( isUserLogged() ){
			
			System.out.println("RICHIESTE DI AMICIZIA:\n" + user.getListaRichiesteDiAmicizia() );
	
		}
		else {
			
			System.out.print("\nNessun utente loggato");
		}
		
		System.out.println("\n------------------------------------------");
	}
	
	@Override
	public String getAddress() throws RemoteException {
		
		if (address == null) {
			
			address = RCLAddress.getAddress("172.241.0.100", port, false);
		}
		return address;
	}
}

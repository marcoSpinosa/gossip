/**
 *  Package per il progetto finale del Laboratorio di programmazione di rete
 */
package it.unipi.di.marcoSpinosa.progettoReti;


/**
 * @author marco
 *
 */
@SuppressWarnings ( "javadoc" )
public class RCLConstant {
	

	public static final boolean DEBUG = true; 
	
	/**
	 *  Informazioni di default
	 */
	public final static String REGISTRY_SERVICE_NAME	=	"RCLGossipServer";
	public final static String REGISTRY_IP				= 	"172.241.0.1";
	public final static int    REGISTRY_PORT 			= 	1099;
	public final static String GOSSIP 					=	"rmi://" + REGISTRY_IP +":"+REGISTRY_PORT+"/" + REGISTRY_SERVICE_NAME;
	
	public final static String TO_PROXY					= 	"toProxy";
	public final static String PROXY_GET_MSG			= 	"proxyGetMsg";
	public final static String PROXY_MSG_LIST			=	"proxyMsgList";
	
	public final static String VBOXNET0					= 	"vboxnet0";
	public final static String ETH						= 	"eth";
	
	public final static String NO_USER 							= "NO_USER";
	public final static int NUMBER_OF_PRESUMED_INITIAL_PROXY 	= 10;
	public final static int INITIAL_NUMBER_OF_USER 				= 50;
	public final static int INITIAL_NUMBER_OF_FRIEND			= 3;
	public final static int DEFAULT_SLEEP_TIME_IN_SEC			= 10;
	public final static String NO_PROXY 						= "noProxy";
	
	/**
	 *  Interfaccia a comandi
	 */
	public final static String REGISTER_COMMAND 	="register";
	public final static String SEND_COMMAND	 		="send";
	public final static String LOGIN_COMMAND	 	="login";
	public final static String LOGOUT_COMMAND	 	="logout";
	public final static String ALLOW_COMMAND	 	="allow";
	public final static String DISALLOW_COMMAND 	="disallow";
	public final static String FRIEND_COMMAND	 	="friend";
	public final static String UNFRIEND_COMMAND	 	="unfriend";
	public final static String HELP_COMMAND	 		="help";
	public final static String PROXY_INFO_COMMAND 	="proxyinfo";
	public final static String NICK_INFO_COMMAND 	="nickinfo";
	public final static String FRIEND_REQUEST 		="friendrequest";
	public final static String NICK_INFO_COMMAND_UA ="nickinfo";
	public final static String ALL_MESSAGE			="allmsg";
	public final static String MESSAGE_FROM			="msgfrom";
	public final static String MESSAGE_FOR			="msgfor";
	public final static String SLEEP_TIME_COMMAND	="sleep";
	
	
	/**
	 *  codici di risposta
	 */
	public final static String USER_ALREADY_REGISTERED 			= "userAlreadyRegistered";
	public final static String UNREGISTERED_USER 				= "unregisteredUser";
	public final static String REGISTRATION_OK 					= "registrationOk";
	public final static String USER_ALREADY_LOGGED 				= "userAlreadyLogged";
	public final static String USER_LOGGED_FROM_DIFFERENT_UA 	= "userLoggedFromDifferentUserAgent";
	public final static String ALLOW_BEFORE_FRIEND 				= "allowBeforeFriend";
	public final static String NO_LOGGED_USER					= "noLoggedUser";
	public final static String LOGGED_USER						= "LoggedUser";
	public final static String LOGIN_OK							= "loginOk";
	public final static String WRONG_NICKNAME					= "wrongNickname";
	
	public final static String FRIEND_OK	 					= "friendOk";
	public final static String FRIEND_NO	 					= "friendNo";
	public final static String UNFRIEND_OK	 					= "unfriendOk";
	public final static String ALLOW_OK	 						= "allowOk"	;
	public final static String ALLOW_FRIEND	 					= "allowFriend"	;
	public final static String DISALLOW_OK	 					= "disallowOk";
	public final static String DISALLOW_NO	 					= "disallowNO";
	public final static String LOGOUT_OK	 					= "logoutOk";
	public final static String SEND_OK 							= "sendOK";
	public final static String SEND_NO 							= "sendNO";
	public final static String WAITING_FOR_ALLOW 				= "waitingForAllow";
	public final static String REALLOW_OK						= "reallowOk";
	public final static String UNFRIEND_NO						= "unfriendNo";
	public final static String BLOCKED_BY_NICKNAME				= "blockedByNickname";
	public final static String BLOCKED_NICKNAME					= "blockedNickname";
	public final static String FRIEND_REQUEST_FROM_NICKNAME		= "friendRequestFromNickname";
	public final static String FRIEND_REQUEST_SENDED			= "friendRequestSended";
	public final static String DENY_FRIEND_OK 					= "denyFriendOk";
	public final static String FRIEND_REQUEST_CANCEL 			= "friendRequestCancel";
	public final static String DISALLOWED_USER 					= "disallowedUser";
	
	public final static String ACTION_AGAINST_YOURSELF 			= "actionAgainstYourself";
	
	
	
	/**
	 *  Funzione che stampa a video le istruzioni dello @ref RCLUserAgent
	 */
	public static void printHelp(){
		System.out.println( REGISTER_COMMAND + " nickname\n -> registra un nuovo nickname" );
		System.out.println( LOGIN_COMMAND 	 + " nickname\n -> collega nickname al servizio" );
		System.out.println( LOGOUT_COMMAND 	 + " nickname\n -> scollega nickname dal servizio" );
		System.out.println( SEND_COMMAND 	 + " nickname\n -> invia a nickname la riga successiva al comando" );
		System.out.println( ALLOW_COMMAND 	 + " nickname\n -> include nickname nella propria lista di ingresso" );
		System.out.println( DISALLOW_COMMAND + " nickname\n -> esclude nickname nella propria lista di ingresso" );
		System.out.println( FRIEND_COMMAND	 + " nickname\n -> include nickname nella propria lista di uscita" );
		System.out.println( UNFRIEND_COMMAND + " nickname\n -> esclude nickname nella propria lista di uscita" );
		System.out.println( HELP_COMMAND 	 + "\n -> Stampa questo aiuto" );
	}
	
	public static void printHelpForDebug(){
		System.out.println( REGISTER_COMMAND 		+ " nickname\n -> registra un nuovo nickname" );
		System.out.println( LOGIN_COMMAND 	 		+ " nickname\n -> collega nickname al servizio" );
		System.out.println( LOGOUT_COMMAND 	 		+ " nickname\n -> scollega nickname dal servizio" );
		System.out.println( SEND_COMMAND 	 		+ " nickname\n -> invia a nickname la riga successiva al comando" );
		System.out.println( ALLOW_COMMAND 	 		+ " nickname\n -> include nickname nella propria lista di ingresso" );
		System.out.println( DISALLOW_COMMAND 		+ " nickname\n -> esclude nickname nella propria lista di ingresso" );
		System.out.println( FRIEND_COMMAND	 		+ " nickname\n -> include nickname nella propria lista di uscita" );
		System.out.println( UNFRIEND_COMMAND 		+ " nickname\n -> esclude nickname nella propria lista di uscita" );
		System.out.println( NICK_INFO_COMMAND_UA 	+ "\n-> stampa le informazioni relative all'utente loggato" );
		System.out.println( HELP_COMMAND 	 		+ "\n -> Stampa questo aiuto" );
	}
	
	public static void printHelpForProxyDebug(){
		System.out.println( ALL_MESSAGE 			+ "\n -> Stampa tutti i messaggi" );
		System.out.println( MESSAGE_FOR 	 		+ "\n -> Stampa tutti i messaggi destinati al nickname scritto nella riga successiva al comando" );
		System.out.println( MESSAGE_FROM 	 		+ "\n -> Stampa tutti i messaggi inviati dal nickname scritto nella riga successiva al comando" );
		System.out.println( HELP_COMMAND 	 		+ "\n -> Stampa questo aiuto" );
	}
	
	/**
	 * Funzione che stampa a video messaggi di errore causati dall'invocazione scorretta dei comandi dello @ref RCLUserAgent
	 * 
	 * @param command comando invocato scorrettamente
	 */
	public static void printError ( final String command ){
		
		switch( command ){
		
		case REGISTER_COMMAND:
				System.out.println("Il comando " + REGISTER_COMMAND + " richiede il nickname dell'utente da registrare.");
				System.out.println("Usa " + HELP_COMMAND + " per visualizzare le istruzioni\n");
				break;
		
		case LOGIN_COMMAND:
				System.out.println("Il comando " + LOGIN_COMMAND + " richiede il nickname dell'utente da loggare.");
				System.out.println("Usa " + HELP_COMMAND + " per visualizzare le istruzioni\n");
				break;
			
		case LOGOUT_COMMAND:	
				System.out.println("Il comando " + LOGOUT_COMMAND + " richiede il nickname dell'utente da disconnettere dal servizio.");
				System.out.println("Usa " + HELP_COMMAND + " per visualizzare le istruzioni\n");
				break;
			
		case SEND_COMMAND:
				System.out.println("Il comando " + SEND_COMMAND + " richiede il nickname del destinatario.");
				System.out.println("Usa " + HELP_COMMAND + " per visualizzare le istruzioni\n");
				break;
			
		case ALLOW_COMMAND:
				System.out.println("Il comando " + ALLOW_COMMAND + " richiede il nickname dell'utente a cui vuoi concedere il permesso di inviarti messaggi.");
				System.out.println("Usa " + HELP_COMMAND + " per visualizzare le istruzioni\n");
				break;
			
		case DISALLOW_COMMAND:
				System.out.println("Il comando " + DISALLOW_COMMAND + " richiede il nickname dell'utente a cui vuoi revocare il permesso di inviarti messaggi.");
				System.out.println("Usa " + HELP_COMMAND + " per visualizzare le istruzioni\n");
				break;
			
		case FRIEND_COMMAND:
				System.out.println("Il comando " + FRIEND_COMMAND + " richiede il nickname dell'utente che desideri aggiungere ai tuoi amici.");
				System.out.println("Usa " + HELP_COMMAND + " per visualizzare le istruzioni\n");
				break;
			
		case UNFRIEND_COMMAND:
				System.out.println("Il comando " + UNFRIEND_COMMAND + " richiede il nickname dell'utente che non desideri più avere tra i tuoi amici.");
				System.out.println("Usa " + HELP_COMMAND + " per visualizzare le istruzioni\n");
				break;
		
		default:
			System.out.println("Comando non valido\nUsa " + HELP_COMMAND + " per visualizzare le istruzioni\n");
			break;
	}
	}
	
	/**
	 *  Funzione che stampa a video le istruzioni del debug dellla @ref Registry
	 */
	public static void printRegistryHelp(){
		
		System.out.println( PROXY_INFO_COMMAND + " -> Visualizza le informazioni relative ai proxy attivi" );
		
		System.out.println( NICK_INFO_COMMAND + " -> Visualizza le info dell 'utente il cui nickname è quello passato nella riga successiva" );
		
		System.out.println( SLEEP_TIME_COMMAND + " -> imposta il valore passato nella riga successiva al comando come secondi di attesa tra un ricerca e l'altra del RCLRegistryIsAliveThread" );
	}	
}

/**
 * 
 */
package it.unipi.di.marcoSpinosa.progettoReti;

import java.util.ArrayList;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;



/**
 * @author marco
 *
 */
public class RCLUserDataStruct implements Cloneable{
	
	
	private final String proxyInfo;
	private ArrayList<String> listaIngresso;
	private ArrayList<String> listaUscita;
	private ArrayList<String> listaRichiesteDiAmicizia;
	private ArrayList<String> listaContattiBloccati;
	private ArrayList<String> bloccatoDa;
	
	private static final String PROXY_INFO 					= "proxyInfo";
	private static final String LISTA_INGRESSO 				= "listaIngresso";
	private static final String LISTA_USCITA 				= "listaUscita";
	private static final String LISTA_RICHIESTE_DI_AMICIZIA = "listaRichiesteDiAmicizia";
	private static final String LISTA_CONTATTI_BLOCCATI 	= "listaContattiBloccati";
	private static final String BLOCCATO_DA 				= "bloccatoDa";
	
	/**
	 * @param proxyInfo
	 */
	public RCLUserDataStruct(final String proxyInfo) {
		super();
		this.proxyInfo 					= proxyInfo;
		this.listaIngresso 				= new ArrayList<String>(RCLConstant.INITIAL_NUMBER_OF_FRIEND);
		this.listaUscita 				= new ArrayList<String>(RCLConstant.INITIAL_NUMBER_OF_FRIEND);
		this.listaContattiBloccati 		= new ArrayList<String>(RCLConstant.INITIAL_NUMBER_OF_FRIEND);
		this.bloccatoDa 				= new ArrayList<String>(RCLConstant.INITIAL_NUMBER_OF_FRIEND);
		this.listaRichiesteDiAmicizia 	= new ArrayList<String>(RCLConstant.INITIAL_NUMBER_OF_FRIEND);
	}
	
	/**
	 * @param proxy
	 */
	@SuppressWarnings("unchecked")
	public RCLUserDataStruct(final JSONObject proxy) {
		super();
		this.proxyInfo 					= (String) proxy.get(PROXY_INFO);
		this.listaIngresso 				= (ArrayList<String>) proxy.get(LISTA_INGRESSO);
		this.listaUscita 				= (ArrayList<String>) proxy.get(LISTA_USCITA);
		this.listaRichiesteDiAmicizia 	= (ArrayList<String>) proxy.get(LISTA_RICHIESTE_DI_AMICIZIA);
		this.listaContattiBloccati 		= (ArrayList<String>) proxy.get(LISTA_CONTATTI_BLOCCATI);
		this.bloccatoDa 				= (ArrayList<String>) proxy.get(BLOCCATO_DA);
	}
	
	/**
	 * @return u
	 */
	@SuppressWarnings("unchecked")
	public JSONObject toJSON() {
		
		JSONObject userData = new JSONObject();
		
		userData.put(PROXY_INFO, this.proxyInfo);
		userData.put(LISTA_INGRESSO, this.listaIngresso);
		userData.put(LISTA_USCITA, this.listaUscita);
		userData.put(LISTA_RICHIESTE_DI_AMICIZIA, this.listaRichiesteDiAmicizia);
		userData.put(LISTA_CONTATTI_BLOCCATI, this.listaContattiBloccati);
		userData.put(BLOCCATO_DA, this.bloccatoDa);
		
		return userData;
	}
	
	/**
	 * @param userInfo
	 * @return p
	 * @throws ParseException 
	 */
	public static RCLUserDataStruct getUserDataFromString(String userInfo) throws ParseException {
	
		JSONObject newUserDataJSON = (JSONObject) new JSONParser().parse( userInfo );
	
		return new RCLUserDataStruct( newUserDataJSON );
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@SuppressWarnings("javadoc")
	public Object clone() { try { return super.clone(); } catch (CloneNotSupportedException e) { e.printStackTrace(); return null; } }
	
	/**
	 * @param newFriend
	 */
	public void addNewFriend(final String newFriend) {
		
		this.getListaUscita().add(newFriend);
//		ArrayList<String> listaUscita = this.getListaUscita();
//		listaUscita.add(newFriend);
//		
//		this.setListaUscita(listaUscita);
	}
	
	/**
	 * @param newFriend
	 */
	public void addFriendRequest(final String newFriend) {
		
		this.getListaRichiesteDiAmicizia().add(newFriend);
		
//		ArrayList<String> listaRichiesteDiAmicizia = this.getListaRichiesteDiAmicizia();
//		listaRichiesteDiAmicizia.add(newFriend);
//		
//		this.setListaRichiesteDiAmicizia(listaRichiesteDiAmicizia);
	}
	
	/**
	 * @param exFriend
	 */
	
	public void removeFriend(final String exFriend) {
		
//		ArrayList<String> listaUscita = this.getListaUscita();
//		ArrayList<String> listaIngresso = this.getListaIngresso();
//		ArrayList<String> listaContattiBloccati = this.getListaContattiBloccati();
//		ArrayList<String> bloccatoDa = this.getBloccatoDa();
//		ArrayList<String> listaRichiesteDiAmicizia = this.getListaRichiesteDiAmicizia();
		
		this.getListaUscita().remove( exFriend );
		this.getListaIngresso().remove( exFriend );
		this.getListaContattiBloccati().remove( exFriend );
		this.getBloccatoDa().remove( exFriend );
		this.getListaRichiesteDiAmicizia().remove( exFriend );
		
//		this.setListaUscita(listaUscita);
//		this.setListaIngresso(listaIngresso);
//		this.setListaContattiBloccati(listaContattiBloccati);
//		this.setBloccatoDa(bloccatoDa);
//		this.setListaRichiesteDiAmicizia( listaRichiesteDiAmicizia );
		
//		ArrayList<String> listaUscita = this.getListaUscita();
//		ArrayList<String> listaIngresso = this.getListaIngresso();
//		ArrayList<String> listaContattiBloccati = this.getListaContattiBloccati();
//		ArrayList<String> bloccatoDa = this.getBloccatoDa();
//		ArrayList<String> listaRichiesteDiAmicizia = this.getListaRichiesteDiAmicizia();
//		
//		listaUscita.remove( exFriend );
//		listaIngresso.remove( exFriend );
//		listaContattiBloccati.remove( exFriend );
//		bloccatoDa.remove( exFriend );
//		listaRichiesteDiAmicizia.remove( exFriend );
//		
//		this.setListaUscita(listaUscita);
//		this.setListaIngresso(listaIngresso);
//		this.setListaContattiBloccati(listaContattiBloccati);
//		this.setBloccatoDa(bloccatoDa);
//		this.setListaRichiesteDiAmicizia( listaRichiesteDiAmicizia );
	}
	
	/**
	 * @param allowedUser
	 */
	public void allowNewFriend(final String allowedUser) {
		
		ArrayList<String> listaIngresso = this.getListaIngresso();
		ArrayList<String> listaRichiesteDiAmicizia = this.getListaRichiesteDiAmicizia();
		
		listaIngresso.add(allowedUser);
		listaRichiesteDiAmicizia.remove(allowedUser);
		
		this.setListaRichiesteDiAmicizia(listaRichiesteDiAmicizia);
		this.setListaIngresso(listaIngresso);
	}
	
	/**
	 * @param blockedFriend
	 */
	public void reallowFriend(final String blockedFriend) {
		
		ArrayList<String> listaContattiBloccati = this.getListaContattiBloccati();
		ArrayList<String> listaIngresso = this.getListaIngresso();
		
		listaContattiBloccati.remove(blockedFriend);
		listaIngresso.add(blockedFriend);
			 
		this.setListaIngresso(listaIngresso);
		this.setListaContattiBloccati(listaContattiBloccati);
	}
	
	/**
	 * @param nickname
	 */
	public void blockFriend(final String nickname) {
		
		ArrayList<String> listaIngresso = this.getListaIngresso();
		ArrayList<String> listaBloccati = this.getListaContattiBloccati();
		
		listaIngresso.remove(nickname);
		listaBloccati.add(nickname);
		
		this.setListaIngresso(listaIngresso);
		this.setListaContattiBloccati(listaBloccati);
	}
	
	/**
	 * @param nickname
	 */
	public void blockFrom(final String nickname) {
		
		ArrayList<String> bloccatoDa = this.getBloccatoDa();
		bloccatoDa.add(nickname);
			 
		this.setBloccatoDa(bloccatoDa);
	}
	
	/**
	 * @param nickname
	 */
	public void unblockFrom(final String nickname) {
		
		ArrayList<String> bloccatoDa = this.getBloccatoDa();
		bloccatoDa.remove(nickname);
			 
		this.setBloccatoDa(bloccatoDa);
	}
	
	
	/**
	 * @return the proxyInfo
	 */
	public String getProxyInfo() {
		return proxyInfo;
	}
	
	
	/**
	 * @return the listaUscita
	 */
	public ArrayList<String> getListaUscita() {
		return listaUscita;
	}
	
	/**
	 * @param listaUscita the listaUscita to set
	 */
	public void setListaUscita(ArrayList<String> listaUscita) {
		this.listaUscita = listaUscita;
	}
	

	/**
	 * @return the listaIngresso
	 */
	public ArrayList<String> getListaIngresso() {
		return listaIngresso;
	}
	
	/**
	 * @param listaIngresso the listaIngresso to set
	 */
	public void setListaIngresso(ArrayList<String> listaIngresso) {
		this.listaIngresso = listaIngresso;
	}

	/**
	 * @return the bloccatoDa
	 */
	public ArrayList<String> getBloccatoDa() {
		return bloccatoDa;
	}

	/**
	 * @param bloccatoDa the bloccatoDa to set
	 */
	public void setBloccatoDa(ArrayList<String> bloccatoDa) {
		this.bloccatoDa = bloccatoDa;
	}

	/**
	 * @return the listaContattiBloccati
	 */
	public ArrayList<String> getListaContattiBloccati() {
		return listaContattiBloccati;
	}

	/**
	 * @param listaContattiBloccati the listaContattiBloccati to set
	 */
	public void setListaContattiBloccati(ArrayList<String> listaContattiBloccati) {
		this.listaContattiBloccati = listaContattiBloccati;
	}
	
	/**
	 * @return s
	 */
	public ArrayList<String> getListaRichiesteDiAmicizia() {
		return this.listaRichiesteDiAmicizia;
	}

	/**
	 * @param listaRichiesteDiAmicizia
	 */
	public void setListaRichiesteDiAmicizia(final ArrayList<String> listaRichiesteDiAmicizia) {
		this.listaRichiesteDiAmicizia = listaRichiesteDiAmicizia;
	}

	/**
	 * 
	 */
	public void stampaUserDetail() {
		System.out.println("USER INFO:");
		System.out.println("PROXY[JSON]: " + this.getProxyInfo());
		System.out.println("LISTA INGRESSO: " + this.getListaIngresso());
		System.out.println("LISTA USCITA: " + this.getListaUscita());
		System.out.println("CONTATTI BLOCCATI: " + this.getListaContattiBloccati());
		System.out.println("BLOCCATO DA: " + this.getBloccatoDa());
		System.out.println("RICHIESTE DA: " + this.getListaRichiesteDiAmicizia() + "\n");
		
	}
}

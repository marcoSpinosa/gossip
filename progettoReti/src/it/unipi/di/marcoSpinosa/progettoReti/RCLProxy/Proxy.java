/**
 * 
 */
package it.unipi.di.marcoSpinosa.progettoReti.RCLProxy;

import it.unipi.di.marcoSpinosa.progettoReti.RCLAddress;
import it.unipi.di.marcoSpinosa.progettoReti.RCLConstant;
import it.unipi.di.marcoSpinosa.progettoReti.RCLMsgDataStruct;
import it.unipi.di.marcoSpinosa.progettoReti.RCLRegistry.RCLRegistryRemoteInterfaceForProxy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.regex.PatternSyntaxException;

import org.json.simple.parser.ParseException;

/**
 * @author marco
 *
 */
public class Proxy {
	
	private static int PROXY_PORT;
	
	private static ConcurrentHashMap<String, List<String>> utentiMap = new ConcurrentHashMap<String, List<String>>(RCLConstant.INITIAL_NUMBER_OF_USER);

	private static BufferedReader input;
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		if (args.length != 1){
			System.out.println("ATTENZIONE!:\nInvocazione errara del proxy");
			System.out.println("USAGE:\njava -jar Proxy <port>");
			System.exit(-1);
		}
		
		try {
			PROXY_PORT = Integer.parseInt(args[0]);
			
		} catch ( NumberFormatException ex ) {
			//Non è stato passato un numero come primo argomento
			System.out.println("ATTENZIONE!:\nNumero di porta non corretto.");
			System.exit(-1);
		}
		
		ServerSocket serverSocket = null;
		
        try {
        	
            serverSocket = new ServerSocket(PROXY_PORT);
            
        } catch (IOException ex) {
            System.err.println("ATTENZIONE!:\nProblemi durante la creazione del socket.");
            System.exit(-1);
            
        } catch (IllegalArgumentException ex) {
            System.err.println("ATTENZIONE!:\nImpossibile mettersi in ascolto sulla porta: " + PROXY_PORT);
            System.exit(-1);
        }
        
        if (RCLConstant.DEBUG) {
        	System.out.println("\n------------------------------------------\n");
        	System.out.println("Socket Creato sulla porta " + serverSocket.getLocalPort() + "\n");
        	System.out.println("Cerco Servizio Gossip...\n");
        }
        
		RCLRegistryRemoteInterfaceForProxy server = null;
		
		try {
				
			 server = (RCLRegistryRemoteInterfaceForProxy) Naming.lookup(RCLConstant.GOSSIP);
			
		} catch ( NotBoundException ex ) {
			//Nome Servizio Sbagiato o non avviato, ma indirizzo e porta corretti con registry attiva
			System.out.println("ATTENZIONE!:\nIl servizio richiesto potrebbe non essere disponibile.\nRiprovare in un altro momento.\n");
			System.exit(-1);
			
		} catch ( RemoteException ex ){ 
			//Di solito Registry non avviato
			System.out.println("ATTENZIONE!:\nImpossibile contattare il server.\n");
			System.exit(-1);
			
		} catch ( MalformedURLException ex ) {
			//Indirizzo passato non è un URL...Per come è strutturato il programma non dovrebbe essere mai lanciata
			System.out.println("ATTENZIONE!:\nURL del server non corretta! \n");
			System.exit(-1);
		} 
		
		if (RCLConstant.DEBUG) System.out.println ("Comunico al server di registrarmi...\n");
				
		try {
			
			server.registerProxy(RCLAddress.getAddress("172.241.0.11", PROXY_PORT, true));
			
		} catch (RemoteException e) {
			
			System.out.println("ATTENZIONE\nProblema di connessione con il server.");
			
			System.exit(-1);
		}
		
		if (RCLConstant.DEBUG) System.out.println("\n------------------------------------------\n");
		
		try {
			
			if (RCLConstant.DEBUG){
				
				RCLProxyDebugThread gestoreSocketDebugThread = new RCLProxyDebugThread( serverSocket, utentiMap );
				
				gestoreSocketDebugThread.start();
				
				input = new BufferedReader(new InputStreamReader(System.in));
				
				String in = null, command= null;
				
				RCLMsgDataStruct msg = null;
			    
			    Enumeration<String> enumeration = null;
					    	
				List<String> messList = null;
					    	
				String nick = null;
				
				String array[] = null;
			
				while ( true ) {
						
						in = input.readLine().toLowerCase();
						
						try{		
								
							array = in.split(" ");																														
								
						} catch (PatternSyntaxException ex ){
							//Regex non valida ...Per come è strutturato il programma non dovrebbe essere mai lanciata
							System.out.println("ATTENZIONE!:\nPattern di riconoscimento comando non valido");
							System.exit(-1);
						}
						
						if (array.length != 1){
							
							System.out.println("ATTENZIONE!:\nTroppi parametri passati");
							System.out.println("Usa " + RCLConstant.HELP_COMMAND + " per visualizzare le istruzioni\n");
							continue;
						}
						
						command = array[0];
						
						switch( command ) {
						
							case RCLConstant.ALL_MESSAGE:
									
									enumeration = utentiMap.keys();
									
									if(!enumeration.hasMoreElements()){
										System.out.println("\n------------------------------------------\n");
							    		System.out.println("Attualmente non sono presenti messaggi\n");
										System.out.println("------------------------------------------");
										break;
									}
								
									while(enumeration.hasMoreElements()) {
										    		
										nick = enumeration.nextElement();
										    		
										messList = utentiMap.get( nick );
										    		
										System.out.println("\n------------------------------------------\nMESSAGGI DESTINATI A " + nick.toUpperCase() +":\n");
										    		
										for ( String mess : messList ) {
							
										    try {
										    				
												msg = RCLMsgDataStruct.getMsgFromString( mess );
														
										    } catch ( ParseException e ) {
											    
										    	System.out.println("...errore durante il parsing di questo messaggio -> " + mess);
											    continue;
											}
							
											System.out.println("DA "+msg.getMittente().toUpperCase() + " : " + msg.getMessaggio() + "\n");
							
										}
										
										System.out.println("------------------------------------------");
									}
									break;
							
									
							case RCLConstant.MESSAGE_FOR:
									
									nick = getNickFromInput();
									
									if (nick == null) {
										continue;
									}
									
									if (utentiMap.containsKey( nick ) ){
										    		
										System.out.println("\n------------------------------------------\nMESSAGGI DESTINATI A " + nick.toUpperCase() +":\n");
										    		
										messList = utentiMap.get( nick );
										
										for ( String mess : messList ) {
							
										    try {
										    				
												msg = RCLMsgDataStruct.getMsgFromString( mess );
														
										    } catch ( ParseException e ) {
											    
										    	System.out.println("...errore durante il parsing di questo messaggio -> " + mess);
											    continue;
											}
							
											System.out.println("DA " + msg.getMittente().toUpperCase() + " : " + msg.getMessaggio() + "\n");
							
										}
										
										System.out.println("------------------------------------------");
										
									} else {
										System.out.println("\n------------------------------------------\nMESSAGGI DESTINATI A " + nick.toUpperCase() +":\n");
										System.out.println("Nessun messaggio destinato a " + nick + "\n------------------------------------------");
									}
									break;
									
									
							case RCLConstant.MESSAGE_FROM:
								
								enumeration = utentiMap.keys();
								
								String nickname = getNickFromInput();
								
								if (nick == null) {
									continue;
								}
								
								System.out.println("\n------------------------------------------\nMESSAGGI INVIATI DA " + nickname.toUpperCase() +":\n");
								
								Boolean ciSonoMsg = false;
							
								while(enumeration.hasMoreElements()) {
									    		
									nick = enumeration.nextElement();
									    		
									messList = utentiMap.get( nick );
									    		
									for ( String mess : messList ) {
						
									    try {
									    				
											msg = RCLMsgDataStruct.getMsgFromString( mess );
													
									    } catch ( ParseException e ) {
										    
									    	System.out.println("...errore durante il parsing di questo messaggio -> " + mess);
										    continue;
										}
						
									    if(msg.getMittente().equals( nickname )){
									    	System.out.println("A "+msg.getDestinatario().toUpperCase() + " : " + msg.getMessaggio() + "\n");
									    	ciSonoMsg = true;
									    }
									}
								}
								if( !ciSonoMsg ){
									
									System.out.println("Non sono presenti messaggi inviati da questo utente\n");
								}
								System.out.println("------------------------------------------");
								break;
							
							case RCLConstant.HELP_COMMAND:
									RCLConstant.printHelpForProxyDebug();
									break;
								
							default:
									System.out.println("Comando non valido\nUsa " + RCLConstant.HELP_COMMAND + " per visualizzare le istruzioni\n");
									break;				
						}
				}
				
			} else {
				
				Executor proxyThreadExecutor = Executors.newCachedThreadPool ();
				
				while ( true ) {
					
						proxyThreadExecutor.execute ( new RCLProxyThread ( serverSocket.accept(), utentiMap ) );
				}
				
			}
	
		} catch (IOException e) {
			
			System.out.println("ATTENZIONE\nproblema con il socket.");
			
		} finally {
			
			try{
				
				serverSocket.close();
				
			} catch ( IOException e ) {
				
				System.out.println("ATTENZIONE\nImpossibile chiudere il socket.");
			}
			
			System.out.println("FINITO!");
			
		}
	}
	
	private static String getNickFromInput(){
		
		String nick = null;
		
		try {
			
			nick = input.readLine().toLowerCase();
		
		} catch ( IOException e ) {
			return null;
		}
		
		String array[] = null;
		
		try{		
			
			array = nick.split(" ");																														
				
		} catch (PatternSyntaxException ex ){
			//Regex non valida ...Per come è strutturato il programma non dovrebbe essere mai lanciata
			System.out.println("ATTENZIONE!:\nPattern di riconoscimento comando non valido");
			System.exit(-1);
		}
		
		if (array.length != 1){
			
			System.out.println("ATTENZIONE!:\nTroppi parametri passati");
			System.out.println("Usa " + RCLConstant.HELP_COMMAND + " per visualizzare le istruzioni\n");
			return null;
		}
		return nick;
	}
}

/*	
{"mittente":"minni","destinatario":"pluto","messaggio":"ciao pluto, sono minni"}
{"mittente":"minni","destinatario":"marco","messaggio":"ciao marco, sono minni"}
{"mittente":"marco","destinatario":"minni","messaggio":"ciao minni, sono marco"}
{"mittente":"marco","destinatario":"pluto","messaggio":"ciao pluto, sono marco"}
{"mittente":"pluto","destinatario":"marco","messaggio":"ciao marco, sono pluto"}
{"mittente":"pluto","destinatario":"minni","messaggio":"ciao minni, sono pluto"}

*/
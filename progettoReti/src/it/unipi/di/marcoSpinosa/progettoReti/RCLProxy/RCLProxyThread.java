/**
 * 
 */
package it.unipi.di.marcoSpinosa.progettoReti.RCLProxy;

import it.unipi.di.marcoSpinosa.progettoReti.RCLConstant;
import it.unipi.di.marcoSpinosa.progettoReti.RCLMsgDataStruct;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;


/**
 * @author marco
 *
 */
public class RCLProxyThread extends Thread {
	
	private final Socket socket;
	private ConcurrentHashMap<String, List<String>> utenti;

    /**
     * @param socket
     * @param utentiMap
     */
    public RCLProxyThread ( final Socket socket, ConcurrentHashMap<String, List<String>> utentiMap ) {
    	super("RCLProxyThread");
    	this.socket = socket;
    	this.utenti = utentiMap;
    }

    /* (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    @SuppressWarnings({ "javadoc", "unchecked" })
	public void run() {
	
		try {
			
			System.out.println("RCLProxyThread: nuova connessione!");
			
		    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
		    
		    BufferedReader in = new BufferedReader( new InputStreamReader( socket.getInputStream() ));
	
		    String inputLine;
		    
		    RCLMsgDataStruct msg = null;
		    
		    //Attendo di ricevere un messaggio
		    while ((inputLine = in.readLine()) != null) {
		    	
		    	//Ricostruisco il messaggio 	
		    	try {
					msg = RCLMsgDataStruct.getMsgFromString(inputLine);
					
				} catch (ParseException e) {
					
					if(RCLConstant.DEBUG){
						
						System.out.println("\n------------------------------------------\n");
						
						System.out.println("Ho ricevuto questo, ma non sembra essere un messaggio...:\n\n" + inputLine + "\n\n...Ignorato\n");
						
						System.out.println("\n------------------------------------------");
					}
					continue;
				}
		    	String user = msg.getDestinatario();
		    	
		    	//Se è una richiesta di download dei messaggi ricevuti mentre si era offline 
		    	if (user.equals( RCLConstant.TO_PROXY )) {
		    		
		    		//Creo la lista 
		    		JSONObject list = new JSONObject();
		    		
		    		//la riempio con i messaggi, se presenti,
		    		if(utenti.containsKey(msg.getMittente()) ){
		    			
		    			list.put(RCLConstant.PROXY_MSG_LIST, utenti.get(msg.getMittente()));
		    			
		    			utenti.remove(msg.getMittente());
		    		}
		    		else{
		    			
		    			list.put(RCLConstant.PROXY_MSG_LIST, new ArrayList<String>());
		    		}
		    		//la ivio e mi rimetto in attesa
		    		out.println(list.toJSONString());
		    		
		    		continue;
		    	}
		    	
		    	//altrimenti si tratta di un messaggio destinato ad un utente offline
		    	List<String> oldList = null;
    			List<String> newList = null;
    			
    			//Se sono presenti messaggi destinati a questo utente
		    	if(utenti.containsKey(user) ){
		    		
		    		//il nuovo messaggio viene concatenato
		    		do {
		    			
		    			oldList = utenti.get(user);
		    			newList = utenti.get(user);
		    			newList.add(inputLine);
		    			
		    		} while (! utenti.replace(user, oldList, newList));
		    		
		    	} else {
		    		
		    		//altrimenti viene creata una nuova lista di messaggi per questo utente
		    		newList = new ArrayList<String>();
		    		
		    		newList.add(inputLine);
		    		
		    		utenti.put(user, newList);
		    	}

		    }
		    
		    System.out.println("Fine");
		    
		    out.close();
		    
		    in.close();
		    
		    socket.close();
	
		} catch (IOException e) {
		   
			e.printStackTrace();   
		}
    }
}

/**
 * 
 */
package it.unipi.di.marcoSpinosa.progettoReti.RCLProxy;


import java.io.IOException;
import java.net.ServerSocket;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * @author marco
 *
 */
public class RCLProxyDebugThread extends Thread {
	
	private ConcurrentHashMap<String, List<String>> utenti;
	private ServerSocket serverSocket;

    /**
     * @param serverSocket 
     * @param utentiMap
     */
    public RCLProxyDebugThread (ServerSocket serverSocket, ConcurrentHashMap<String, List<String>> utentiMap ) {
    	super("RCLProxyDebugThread");
    	this.utenti = utentiMap;
    	this.serverSocket = serverSocket;
    }

    /* (non-Javadoc)
     * @see java.lang.Thread#run()
     */
	@SuppressWarnings ( "javadoc" )
	public void run() {
		
		System.out.println("\n------------------------------------------\n");
		
		System.out.println("RCLProxyDebugThread: Creo il pool di thread...");
		
		Executor proxyThreadExecutor = Executors.newCachedThreadPool ();
		
		System.out.println("RCLProxyDebugThread: Attendo connessioni...");
		
		System.out.println("\n------------------------------------------\n");
		
		while ( true ) {
			
			try {
				
				proxyThreadExecutor.execute ( new RCLProxyThread ( this.serverSocket.accept(), this.utenti ) );
			
			} catch ( IOException e ) {
				System.out.println("Problema con il socket...");
				System.exit( -1 );
			}
		}
	}

}

/**
 * 
 */
package it.unipi.di.marcoSpinosa.progettoReti.RCLProxy;

import it.unipi.di.marcoSpinosa.progettoReti.RCLAddress;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @author marco
 *
 */
public class RCLProxyDataStruct {
	
	private final RCLAddress address;
	private int carico;
//	private PrintWriter output;
	
	/**
	 * @param address 
	 */
	public RCLProxyDataStruct ( final RCLAddress address ) {
		super();
		this.address = address;
		this.carico = 0;
//		this.output = null;
	}
	
	/**
	 * @param proxy
	 * @throws ParseException 
	 */
	public RCLProxyDataStruct ( final JSONObject  proxy) throws ParseException{
		super();
		this.address = RCLAddress.getAddressFromString(proxy.toJSONString());
		this.carico = 0;
//		this.output = null;
	}
	
	/**
	 * @return p
	 */
	public JSONObject toJSON(){
		
		return this.address.toJSON();
	}
	
	/**
	 * @param proxyInfo
	 * @return p
	 * @throws ParseException 
	 */
	public static RCLProxyDataStruct getProxyFromString(String proxyInfo) throws ParseException {
	
		JSONObject newProxyJSON = (JSONObject) new JSONParser().parse( proxyInfo );
	
		return new RCLProxyDataStruct( newProxyJSON );
	}
	
	/**
	 * @return the ip
	 */
	public String getIP() {
		return this.address.getIP();
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return this.address.getPort();
	}
	
	/**
	 * @return the carico
	 */
	public int getCarico() {
		return this.carico;
	}
	
	/**
	 * @param carico the carico to set
	 */
	public void setCarico(int carico) {
		this.carico = carico;
	}

//	/**
//	 * @return the output
//	 */
//	public PrintWriter getOutput() {
//		return this.output;
//	}

//	/**
//	 * @param output the socket to set
//	 */
//	public void setOutput(PrintWriter output) {
//		this.output  = output;
//	}

}

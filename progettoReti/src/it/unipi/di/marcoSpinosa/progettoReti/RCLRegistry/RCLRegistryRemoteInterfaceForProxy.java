/**
 * Package contenenti le classi per la realizzazione del componente Registry del servizio GOSSIP
 */
package it.unipi.di.marcoSpinosa.progettoReti.RCLRegistry;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interfaccia remota utilizzata dal proxy per creare uno stub locale della registry
 * @author Marco Spinosa 477385
 */
public interface RCLRegistryRemoteInterfaceForProxy extends Remote {
	
	/** 
	 * Funzione che permette ad un Proxy di registrarsi presso la Registry
	 * ed essere così inserito nel sistema GOSSIP
	 * 
	 * @param proxyInfo Stringa che rappresenta le informazioni del Proxy da registrare; tipicamente RCLProxyDataStruct.toString
	 * 
	 * @throws RemoteException
	 */
	public void registerProxy ( String proxyInfo ) throws RemoteException;
	
}

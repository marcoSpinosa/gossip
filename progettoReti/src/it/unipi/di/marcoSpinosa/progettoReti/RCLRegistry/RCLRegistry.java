/**
 *  Package per il progetto finale del Laboratorio di programmazione di rete
 */
package it.unipi.di.marcoSpinosa.progettoReti.RCLRegistry;

import it.unipi.di.marcoSpinosa.progettoReti.RCLAddress;
import it.unipi.di.marcoSpinosa.progettoReti.RCLConstant;
import it.unipi.di.marcoSpinosa.progettoReti.RCLUserDataStruct;
import it.unipi.di.marcoSpinosa.progettoReti.RCLProxy.RCLProxyDataStruct;
import it.unipi.di.marcoSpinosa.progettoReti.RCLUserAgent.RCLUserAgentRemoteInterface;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

import org.json.simple.parser.ParseException;





/**
 * @author marco
 *
 */
public class RCLRegistry extends UnicastRemoteObject implements RCLRegistryRemoteInterface, RCLRegistryRemoteInterfaceForProxy {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Array di Proxy
	 */
	private ArrayList<RCLProxyDataStruct> proxyArray = new ArrayList<RCLProxyDataStruct>(RCLConstant.NUMBER_OF_PRESUMED_INITIAL_PROXY);
	
	/**
	 * Semaforo associato all'array di proxy
	 */
	private ReentrantLock proxyLock = new ReentrantLock();
	
	/**
	 * Tabella hash per gli utenti registrati  < NickName, StrutturaDatiUtente>
	 */
	private ConcurrentHashMap<String, RCLUserDataStruct> utentiMap = new ConcurrentHashMap<String, RCLUserDataStruct>(RCLConstant.INITIAL_NUMBER_OF_USER);
	
	/**
	 * Tabella hash per gli userAgent attivi  < NickName loggato, InterfacciaRemotaUserAgent>
	 */
	private ConcurrentHashMap<String, RCLUserAgentRemoteInterface> userAgentMap = new ConcurrentHashMap<String, RCLUserAgentRemoteInterface>(RCLConstant.INITIAL_NUMBER_OF_USER);
	
	/**
	 * Executor per la gestione dei thread per le notifiche
	 */
	private Executor notifyThreadExecutor = Executors.newCachedThreadPool();
	
	
	private int sleepTime = RCLConstant.DEFAULT_SLEEP_TIME_IN_SEC * 1000;
	
	/**
	 * enumerazione stati utente
	 */
	private enum RCLStatus {ONLINE, OFFLINE};
	
	protected RCLRegistry() throws RemoteException {
		super();
		RCLRegistryIsAliveThread thread = new RCLRegistryIsAliveThread();
		thread.setDaemon( true );
		thread.start();
	}

	/* (non-Javadoc)
	 * @see it.unipi.di.marcoSpinosa.progettoReti.RCLRegistryRemoteInterface#register(java.lang.String)
	 */
	@SuppressWarnings("javadoc")
	@Override
	public String register (final String nickname) throws RemoteException {
		
		if (RCLConstant.DEBUG) {
        	System.out.println("\n------------------------------------------");
        	System.out.println("REGISTER");
        }
		
		if (utentiMap.containsKey(nickname)) {
			
			System.out.println("Il comando " + RCLConstant.REGISTER_COMMAND + " è stato invocato per registrare " + nickname + " ma è un utente già registrato");
			
			if (RCLConstant.DEBUG) {
	        	System.out.println("------------------------------------------\n");
	        }
			
			return RCLConstant.USER_ALREADY_REGISTERED;
		}
		
		String proxyInfo = scegliProxy();
		
		if ( !RCLConstant.NO_PROXY.equals( proxyInfo )) {
			
			utentiMap.put(nickname, new RCLUserDataStruct(proxyInfo));
			
			System.out.println("Il comando " + RCLConstant.REGISTER_COMMAND + " è stato invocato per registrare " + nickname + ".\nProxy assegnato [JSON String]:\n" + proxyInfo + "\n");
			
			if (RCLConstant.DEBUG) {
	        	System.out.println("------------------------------------------\n");
	        }
			
			return RCLConstant.REGISTRATION_OK;
		}
		
		if (RCLConstant.DEBUG) {
        	System.out.println("------------------------------------------\n");
        }
		
		return RCLConstant.NO_PROXY;
	}


	/* (non-Javadoc)
	 * @see it.unipi.di.marcoSpinosa.progettoReti.RCLRegistryRemoteInterface#login(java.lang.String, it.unipi.di.marcoSpinosa.progettoReti.RCLUserAgentIF)
	 */
	@SuppressWarnings("javadoc")
	@Override
	public String login ( final String nickname, final RCLUserAgentRemoteInterface userAgent) throws RemoteException {
		
		if (RCLConstant.DEBUG) {
        	System.out.println("\n------------------------------------------\n");
        	System.out.println("LOGIN");	
        }
		
		System.out.print("Il comando " + RCLConstant.LOGIN_COMMAND + " è stato invocato per loggare " + nickname + "...");
		
		RCLUserDataStruct infoNickname = utentiMap.get(nickname);
		
		if ( infoNickname == null ) {
			System.out.println(" utente non registrato al servizio.");
			
			if (RCLConstant.DEBUG) {
				
	        	System.out.println("\n------------------------------------------\n");
	        }
			
			return RCLConstant.UNREGISTERED_USER;
		}
		
		RCLUserAgentRemoteInterface ua = userAgentMap.get(nickname);
		
		if ( ua != null) {
			
			try {
				
				System.out.print(nickname + " utente già loggato...");
				
				userAgentMap.get(nickname).isAlive();
			
			} catch (RemoteException e) {
				//userAgent chiuso senza fare il logout
				System.out.println("ma il suo user agent è morto.\nLoggo con nuovo userAgent");
				
				userAgentMap.replace(nickname, userAgent);
				
				notifyThreadExecutor.execute( new RCLRegistryNotifyThread( nickname, RCLStatus.ONLINE ) );
				
				if (RCLConstant.DEBUG) {
					
		        	System.out.println("\n------------------------------------------\n");	
		        }
				
				return infoNickname.toJSON().toJSONString();
			}
			System.out.println("e il suo userAgent è ancora vivo.");
			
			if (RCLConstant.DEBUG) {
				
	        	System.out.println("\n------------------------------------------\n");	
	        }
			
			return RCLConstant.USER_LOGGED_FROM_DIFFERENT_UA;
		}
		
		userAgentMap.put(nickname, userAgent);
		
		if (RCLConstant.DEBUG) {
			
        	System.out.println("------------------------------------------\n");
        }
		
		notifyThreadExecutor.execute( new RCLRegistryNotifyThread( nickname, RCLStatus.ONLINE ) );
		
		return infoNickname.toJSON().toJSONString();
	}
	
	
	/* (non-Javadoc)
	 * @see it.unipi.di.marcoSpinosa.progettoReti.RCLRegistryRemoteInterface#logout(java.lang.String)
	 */
	@SuppressWarnings("javadoc")
	@Override
	public String logout ( final String nickname ) throws RemoteException {
		
		if (RCLConstant.DEBUG) {
        	
			System.out.println("\n------------------------------------------\n");
        	
        	System.out.println("LOGOUT");	
        }
		
		System.out.println("Il comando " + RCLConstant.LOGOUT_COMMAND + " è stato invocato per disconnettere " + nickname + ".");
		
		if (RCLConstant.DEBUG) {
			
        	System.out.println("------------------------------------------\n");	
        }
		
		notifyThreadExecutor.execute( new RCLRegistryNotifyThread( nickname, RCLStatus.OFFLINE ) );
		
		userAgentMap.remove(nickname);
		
		return RCLConstant.LOGOUT_OK;
	}


	/* (non-Javadoc)
	 * @see it.unipi.di.marcoSpinosa.progettoReti.RCLRegistryRemoteInterface#allow(java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("javadoc")
	@Override
	public String allow( final String nickname, final String allowedUser ) throws RemoteException {
		
		if (RCLConstant.DEBUG) {
        	
			System.out.println("\n------------------------------------------\n");
			
        	System.out.println("ALLOW");	
        }
		
		System.out.println("Il comando " + RCLConstant.ALLOW_COMMAND + " è stato invocato da " + nickname + " per accettare la richiesta di amicizia di "+ allowedUser + ".");
		
//		if (!utentiMap.containsKey(allowedUser)) {
//			lockUtenti.unlock();
//			return RCLConstant.UNREGISTERED_USER;
//		}
//		NO CANCELLAZIONE + CONTROLLI USER AGENT

		RCLUserDataStruct userOld = null;
		RCLUserDataStruct userNew = null;
		RCLUserDataStruct newAllowedUserOld = null;
		RCLUserDataStruct newAllowedUserNew = null;
		
		do {
			
			userOld = utentiMap.get(nickname);
			userNew = (RCLUserDataStruct) userOld.clone();
			
			newAllowedUserOld = utentiMap.get(allowedUser);
			newAllowedUserNew = (RCLUserDataStruct) newAllowedUserOld.clone();
					
			userNew.allowNewFriend(allowedUser);
			userNew.addNewFriend(allowedUser);									//Friend Automatica al contrario
			newAllowedUserNew.allowNewFriend(nickname);							//Allow AUtomatica
			
		} while ( !utentiMap.replace(nickname, userOld, userNew) && !utentiMap.replace(allowedUser, newAllowedUserOld, newAllowedUserNew) );
		
		RCLUserAgentRemoteInterface userAgent = userAgentMap.get(allowedUser);
		
		if (userAgent != null) {
			try {
				
				userAgent.notifyAllowFrom(nickname);
			
			} catch(RemoteException e) {
				
				System.out.println("L'utente " + allowedUser + " si è disconnesso prima che gli potetesse essere inviata la notifica");
				
				notifyThreadExecutor.execute( new RCLRegistryNotifyThread( allowedUser, RCLStatus.OFFLINE ) );
				
				userAgentMap.remove(allowedUser); //Se lo user agent è stato chiuso senza fare il logout
			}
		}
		
		if ( RCLConstant.DEBUG ) {
			
			System.out.println("INFO AGGIORNATE:\n");
			
			System.out.println(nickname);
			
			userNew.stampaUserDetail();
			
			System.out.println(allowedUser);
			
			newAllowedUserNew.stampaUserDetail();
			
			System.out.println("------------------------------------------\n");
		}		
		
		return RCLConstant.ALLOW_OK;
		
	}
	
	/* (non-Javadoc)
	 * @see it.unipi.di.marcoSpinosa.progettoReti.RCLRegistryRemoteInterface#reallow(java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("javadoc")
	@Override
	public String reallow( final String nickname, final String allowedUser ) throws RemoteException {
		
		if (RCLConstant.DEBUG) {
        	
			System.out.println("\n------------------------------------------\n");
			
        	System.out.println("REALLOW");	
        }
		
		System.out.println("Il comando " + RCLConstant.ALLOW_COMMAND + " è stato invocato per permettere a " + nickname + " di sbloccare "+ allowedUser + ".");
		
		RCLUserDataStruct userOld = null;
		RCLUserDataStruct userNew = null;
		RCLUserDataStruct newAllowedUserOld = null;
		RCLUserDataStruct newAllowedUserNew = null;
		
		do {
			
			userOld = utentiMap.get(nickname);
			userNew = (RCLUserDataStruct) userOld.clone();
			
			newAllowedUserOld = utentiMap.get(allowedUser);
			newAllowedUserNew = (RCLUserDataStruct) newAllowedUserOld.clone();
			
			userNew.reallowFriend(allowedUser);
			newAllowedUserNew.unblockFrom(nickname);
			
		} while (!utentiMap.replace(nickname, userOld, userNew) && !utentiMap.replace(allowedUser, newAllowedUserOld, newAllowedUserNew) ); 
				
		System.out.println(nickname + " ha sbloccato " + allowedUser);
					
		RCLUserAgentRemoteInterface userAgent = userAgentMap.get(allowedUser);
		
		if (userAgent != null) {
			try{
				
				userAgent.notifyReallowFrom(nickname);
			
			}catch(RemoteException e){
				
				System.out.println(allowedUser + " si è disconnesso prima che gli potetesse essere inviata la notifica");
				
				notifyThreadExecutor.execute( new RCLRegistryNotifyThread( allowedUser, RCLStatus.OFFLINE ) );
				
				userAgentMap.remove(allowedUser); //Se lo user agent è stato chiuso senza fare il logout
			}
		}
		
		if ( RCLConstant.DEBUG ) {
			
			System.out.println("INFO AGGIORNATE:\n");
			
			System.out.println(nickname);
			
			userNew.stampaUserDetail();
			
			System.out.println(allowedUser);
			
			newAllowedUserNew.stampaUserDetail();
			
			System.out.println("------------------------------------------\n");
		}		
		
		return RCLConstant.REALLOW_OK;
		
	}


	/* (non-Javadoc)
	 * @see it.unipi.di.marcoSpinosa.progettoReti.RCLRegistryRemoteInterface#disallow(java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("javadoc")
	@Override
	public String disallow ( final String nickname, final String disallowedUser ) throws RemoteException {
		
		if (RCLConstant.DEBUG) {
        	
			System.out.println("\n------------------------------------------\n");
			
        	System.out.println("DISALLOW");	
        }
		
		System.out.println("Il comando " + RCLConstant.DISALLOW_COMMAND + " è stato invocato per impedire a " + disallowedUser + " di comunicare con "+ nickname + ".");
		
//		if (!utentiMap.containsKey(disallowedUser)) {
//			return RCLConstant.UNREGISTERED_USER;
//		}
		
		RCLUserDataStruct userOld = null;
		RCLUserDataStruct userNew = null;
		RCLUserDataStruct blockedUserOld = null;
		RCLUserDataStruct blockedUserNew =  null;
		
		do{
			
			userOld = utentiMap.get(nickname);
			userNew = (RCLUserDataStruct) userOld.clone();
			blockedUserOld = utentiMap.get(disallowedUser);
			blockedUserNew = (RCLUserDataStruct) blockedUserOld.clone();
			
			userNew.blockFriend(disallowedUser);
			blockedUserNew.blockFrom(nickname);
			
		} while( !utentiMap.replace(nickname, userOld, userNew) && !utentiMap.replace(disallowedUser, blockedUserOld, blockedUserNew));
		
		RCLUserAgentRemoteInterface userAgent = userAgentMap.get(disallowedUser);
		
		if (userAgent != null) {
			try{
				
				userAgent.notifyDisallowFrom(nickname);
			
			}catch(RemoteException e){
				System.out.println( disallowedUser + " si è disconnesso prima che gli potetesse essere inviata la notifica" );
				
				notifyThreadExecutor.execute( new RCLRegistryNotifyThread( disallowedUser, RCLStatus.OFFLINE ) );
				
				userAgentMap.remove( disallowedUser ); //Se lo user agent è stato chiuso senza fare il logout
			}
		}
		if ( RCLConstant.DEBUG ) {
			
			System.out.println("INFO AGGIORNATE:\n");
			
			System.out.println(nickname);
			
			userNew.stampaUserDetail();
			
			System.out.println(disallowedUser);
			
			blockedUserNew.stampaUserDetail();;
			
			System.out.println("------------------------------------------\n");
		}
		
		return RCLConstant.DISALLOW_OK;
	}


	/* (non-Javadoc)
	 * @see it.unipi.di.marcoSpinosa.progettoReti.RCLRegistryRemoteInterface#friend(java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("javadoc")
	@Override
	public String friend ( final String nickname, final String newFriend ) throws RemoteException {
		
		if (RCLConstant.DEBUG) {
        	
			System.out.println("\n------------------------------------------\n");
			
        	System.out.println("FRIEND");	
        }
		
		System.out.print( nickname + " ha inviato una richiesta di amicizia a " + newFriend + "...");
		
		if (!utentiMap.containsKey(newFriend)) {
			System.out.println( "ma " + nickname + " non è un utente registrato al servizio.");
			return RCLConstant.UNREGISTERED_USER;
		}
		
		System.out.println( "" );
		
		RCLUserDataStruct userOld = null;
		RCLUserDataStruct userNew = null;
		RCLUserDataStruct friendOld = null;
		RCLUserDataStruct friendNew = null;
		
		do {
			
			userOld = utentiMap.get(nickname);
			userNew = (RCLUserDataStruct) userOld.clone();
			
			friendOld = utentiMap.get(newFriend);
			friendNew = (RCLUserDataStruct) friendOld.clone();
			
//			if ( user == null ) {
//				//NON dovrebbe accadere mai..lo User-Agent dovrebbe impedirlo
//				System.out.println(nickname + " non è registrato al servizio. Non può aver invocato la friend");
//				return RCLConstant.UNREGISTERED_USER;
//			}
			
			userNew.addNewFriend(newFriend);
			friendNew.addFriendRequest(nickname);
			
		} while (!utentiMap.replace(nickname, userOld, userNew) && !utentiMap.replace(newFriend, friendOld, friendNew));
		
		
		RCLUserAgentRemoteInterface userAgent = userAgentMap.get(newFriend);
		
		if (userAgent != null) {
			try{
				
				userAgent.notifyFriendFrom(nickname);
			
			}catch(RemoteException e){
				System.out.println("L'utente " + newFriend + " si è disconnesso prima che gli potetesse essere inviata la notifica");
				
				notifyThreadExecutor.execute( new RCLRegistryNotifyThread( newFriend, RCLStatus.OFFLINE ) );
				
				userAgentMap.remove(newFriend); //Se lo user agent è stato chiuso senza fare il logout
			}
		}
		
		if ( RCLConstant.DEBUG ) {
			
			System.out.println("INFO AGGIORNATE:\n");
			
			System.out.println(nickname);
			
			userNew.stampaUserDetail();
			
			System.out.println(newFriend);
			
			friendNew.stampaUserDetail();;
			
			System.out.println("------------------------------------------\n");
		}
		
		return RCLConstant.FRIEND_OK;
		
	}


	/* (non-Javadoc)
	 * @see it.unipi.di.marcoSpinosa.progettoReti.RCLRegistryRemoteInterface#unfriend(java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("javadoc")
	@Override
	public String unfriend ( final String nickname, final String exFriend ) throws RemoteException {
		
		if (RCLConstant.DEBUG) {
        	
			System.out.println("\n------------------------------------------\n");
			
        	System.out.println("UNFRIEND");	
        }

		System.out.println("Il comando " + RCLConstant.UNFRIEND_COMMAND + " è stato invocato per cancellare " + exFriend + " dagli amici di "+ nickname + ".");
		
//		if (!utentiMap.containsKey(exFriend)) {
//			return RCLConstant.UNREGISTERED_USER;
//		}
//		SE è stata consentita dallo user agent allora xeFriend è registrato al servizio
		
		RCLUserDataStruct userOld = null;
		RCLUserDataStruct userNew = null;
		RCLUserDataStruct unfriendUserOld = null;
		RCLUserDataStruct unfriendUserNew = null;
		
		do {
			
			userOld = utentiMap.get(nickname);
			userNew = (RCLUserDataStruct) userOld.clone();
			
			unfriendUserOld = utentiMap.get(exFriend);
			unfriendUserNew = (RCLUserDataStruct) unfriendUserOld.clone();
			
			userNew.removeFriend(exFriend);
			unfriendUserNew.removeFriend(nickname);
			
		} while (!utentiMap.replace(nickname, userOld, userNew) && !utentiMap.replace(exFriend, unfriendUserOld, unfriendUserNew));
		
		RCLUserAgentRemoteInterface userAgent = userAgentMap.get(exFriend);
		
		if (userAgent != null) {
			try {
				
				userAgent.notifyUnfriendFrom(nickname);
			
			} catch ( RemoteException e ) {
				System.out.println("L'utente " + exFriend + " si è disconnesso prima che gli potetesse essere inviata la notifica");
				
				notifyThreadExecutor.execute( new RCLRegistryNotifyThread( exFriend, RCLStatus.OFFLINE ) );
				
				userAgentMap.remove(exFriend); //Se lo user agent è stato chiuso senza fare il logout
			}
		}
		if ( RCLConstant.DEBUG ) {
			
			System.out.println("INFO AGGIORNATE:\n");
			
			System.out.println(nickname);
			
			userNew.stampaUserDetail();
			
			System.out.println(exFriend);
			
			unfriendUserNew.stampaUserDetail();;
			
			System.out.println("------------------------------------------\n");
		}
		
		return RCLConstant.UNFRIEND_OK;
	}



	/* (non-Javadoc)
	 * @see it.unipi.di.marcoSpinosa.progettoReti.RCLRegistryRemoteInterfaceForProxy#register(java.lang.String, it.unipi.di.marcoSpinosa.progettoReti.RCLProxyRemoteInterface)
	 */
	@SuppressWarnings("javadoc")
	@Override
	public void registerProxy ( final String proxyInfo ) throws RemoteException {
		
		if (RCLConstant.DEBUG) {
        	
			System.out.println("\n------------------------------------------\n");
			
        	System.out.println("REGISTER PROXY");	
        }
		
		RCLProxyDataStruct newProxy = null;
		
		try {
			
			newProxy = RCLProxyDataStruct.getProxyFromString(proxyInfo);
			
		} catch (ParseException e) {
			System.out.println("Impossibile registrare il proxy, le informazioni ricevute non sono parsabili");
			e.getErrorType();
			e.getPosition();
			e.getLocalizedMessage();
			return;
		}
		
		proxyLock.lock();
		
			proxyArray.add(newProxy);
			
		proxyLock.unlock();
		
		if (RCLConstant.DEBUG) {
			
			System.out.println("Nuovo Proxy aggiunto\n");
        	
			System.out.println("------------------------------------------\n");	
        }
	}
	

	/**
	 * 
	 * @return
	 */
	private String scegliProxy(){
		
		if ( proxyArray.isEmpty()) {
			
			System.out.println("ATTENZIONE!:\nE' stato richiesto un proxy ma al momento nessuno è attivo");
			
			return RCLConstant.NO_PROXY;
		}
		
		RCLProxyDataStruct p = null;
		
		proxyLock.lock(); 														//fail-fast iterator -> CONCURRENT_MODIFICATION_EXCEPTION
		
			RCLProxyDataStruct proxyScelto = proxyArray.get(0);
			
			Iterator<RCLProxyDataStruct> iteratore = proxyArray.iterator();
			
			while ( iteratore.hasNext() ) {
				
				p = (RCLProxyDataStruct) iteratore.next();
				
				if ( p.getCarico() < proxyScelto.getCarico() ) {
					
					proxyScelto = p;
				}
			}
		
			proxyScelto.setCarico( proxyScelto.getCarico() + 1 );
		
		proxyLock.unlock();
		
		return proxyScelto.toJSON().toJSONString();
		
	}
	
	/**
	 * 
	 */
	public void infoProxy(){
		
		if (! RCLConstant.DEBUG) return;
		
		if ( proxyArray.isEmpty()) {
			System.out.println("\n------------------------------------------");
			System.out.println("Nessun proxy attivo...");
			System.out.println("------------------------------------------");
			return;
		}
		
		RCLProxyDataStruct p = null;
		
		proxyLock.lock(); 														//fail-fast iterator -> CONCURRENT_MODIFICATION_EXCEPTION
		
			Iterator<RCLProxyDataStruct> iteratore = proxyArray.iterator();
			
			System.out.println("\n------------------------------------------");
			System.out.println("PROXY CONNESSI :\n");
			
			while ( iteratore.hasNext() ) {
				
				p = (RCLProxyDataStruct) iteratore.next();
				
				System.out.println("IP:" + p.getIP() + "\nPORT:" + p.getPort() + "\nCARICO:" + p.getCarico() + "\n");
			}
		
		proxyLock.unlock();
		
		System.out.println("------------------------------------------\n");
		
	}
	
	
	/* (non-Javadoc)
	 * @see it.unipi.di.marcoSpinosa.progettoReti.RCLRegistryRemoteInterface#getInfoByNick(java.lang.String)
	 */
	@SuppressWarnings ( "javadoc" )
	public String getAddressByNickString (final String nickname) {
		
		if (RCLConstant.DEBUG) {
        	
			System.out.println("\n------------------------------------------");
			
			System.out.println("GET ADDRESS BY NICK");
			
			System.out.println("cerco info sull'utente " + nickname );
        }
		
		if (userAgentMap.containsKey(nickname)){
			
			if (RCLConstant.DEBUG) {
	        	
				System.out.println("utente online\ngetAddress..");
	        }
			
			try {
				
				if (RCLConstant.DEBUG) {
		        	
					System.out.println("utente online\ngetAddress..." + userAgentMap.get(nickname).getAddress());
		        }
				
				return userAgentMap.get(nickname).getAddress();
				
			} catch (RemoteException e) {
				
				if (RCLConstant.DEBUG) {
		        	
					System.out.print("User Agent di " + nickname + " morto. -> ");
		        }
			}
		}
		
		
		if (RCLConstant.DEBUG) {
			
			System.out.println("utente non online\ngetProxyInfo..."+ utentiMap.get(nickname).getProxyInfo());
        }
		
		return utentiMap.get(nickname).getProxyInfo();
	
	}
	
	
	/**
	 * @param nickname
	 */
	public void getInfoByNickForRegistry (final String nickname) {
		
		if (utentiMap.containsKey( nickname )){
		
			System.out.println("\n------------------------------------------");
			System.out.println("INFO " + nickname.toUpperCase() + " :\n");
			
			RCLUserDataStruct user = utentiMap.get(nickname);
			
			if (userAgentMap.containsKey(nickname)){
				
				System.out.println("utente online...getAddress...");
				
				String add = null;
				
				try {
					
					add = userAgentMap.get(nickname).getAddress();
					RCLAddress address =RCLAddress.getAddressFromString( add );
					
					System.out.println(nickname +" è contattabile a questo indirizzo: " + address.getIP() + ":" + address.getPort() + "\n");
					
					System.out.println("ALTRE INFO:\n");
					user.stampaUserDetail();
					System.out.println("\n------------------------------------------");
					return;
					
				} catch (RemoteException e) {
					System.out.print("User Agent di " + nickname + " morto. -> ");
					
				} catch ( ParseException e ) {
					System.out.print(nickname +" è contattabile a questo indirizzo:\n" + add + "\n");
				}
			}
			
			System.out.println("utente non online...\n");
			
			System.out.println("ALTRE INFO:\n");
			user.stampaUserDetail();
			System.out.println("\n------------------------------------------");
			return;
		}
		System.out.print(nickname + " non è un utente del servizio " + RCLConstant.REGISTRY_SERVICE_NAME);
	}
	
	/**
	 * @param newSleepTime
	 */
	public void setSleepTimeForIsAliveThread(int newSleepTime){
		System.out.println( "\n------------------------------------------" );
		System.out.println( "SleepTime from " + sleepTime/1000 + "sec to " + newSleepTime + "sec.");
		sleepTime = newSleepTime * 1000;
		System.out.println( "------------------------------------------\n" );
	}
	

	/**
	 * @author marco
	 *
	 */
	private class RCLRegistryNotifyThread extends Thread {
		
		private String userNickname;
		private RCLStatus newStatus;
		/**
		 * @param userNickname  
		 * @param newStatus 
		 * 
		 */
		public RCLRegistryNotifyThread ( String userNickname, RCLStatus newStatus ){
			super();
			this.userNickname = userNickname;
			this.newStatus = newStatus;
		}
		
		public void run() {
			
			if (RCLConstant.DEBUG) {
				
				System.out.println("\n------------------------------------------");
				System.out.println("RCLRegistryNotifyThread start: " + userNickname + " " +newStatus );
			}
			RCLUserDataStruct user = (RCLUserDataStruct) utentiMap.get( userNickname ).clone();
			ArrayList<String> friend =  user.getListaUscita();
			Iterator<String> iteratore = friend.iterator();
			String nickname = null;
			String indirizzo = null;
			
			if (newStatus.equals(RCLStatus.ONLINE)){
			
				try {
					if (RCLConstant.DEBUG) {
						System.out.println("ottengo userAgent...");
					}
					
					RCLUserAgentRemoteInterface u = userAgentMap.get(userNickname);
					
					if (RCLConstant.DEBUG) {
						
						System.out.println("ottengo indirizzo...");
					}
					indirizzo = u.getAddress();
					
					if (RCLConstant.DEBUG) {
						System.out.println("Fatto...");
					}
				} catch (RemoteException e1) {
					
					if (RCLConstant.DEBUG) {
						System.out.println("RCLRegistryNotifyThread: " + userNickname + " non è più connesso al servizio.");
					}
					userAgentMap.remove( userNickname );
					
					return;
					
				} catch (NullPointerException e1) {
					if (RCLConstant.DEBUG) {
						System.out.println("RCLRegistryNotifyThread: " + userNickname + " non è più connesso al servizio. -> NULL");
					}
					return;
				}
			}
			
			while(iteratore.hasNext()){
				
				nickname = iteratore.next();
				
				if ( !user.getBloccatoDa().contains(nickname) && user.getListaIngresso().contains(nickname)) {
					
					if ( userAgentMap.containsKey(nickname) ) {
						
						if (RCLConstant.DEBUG) {
							
							System.out.println("RCLRegistryNotifyThread: invio notifica a " + nickname + ".");
						}
						RCLUserAgentRemoteInterface userAgent = userAgentMap.get(nickname);

						try {
							
							if (newStatus.equals(RCLStatus.ONLINE)){
								
								userAgent.notifyUserOnline(userNickname, indirizzo);
		
							} else {
								userAgent.notifyUserOffline(userNickname);
							}
							
						} catch (RemoteException e) {
							
							if (RCLConstant.DEBUG) {
								System.out.println(nickname + " non è più connesso al servizio.");
							}
							notifyThreadExecutor.execute( new RCLRegistryNotifyThread( nickname, RCLStatus.OFFLINE ) );
							
							userAgentMap.remove( nickname );
						}
					}
				}
			}
			if (RCLConstant.DEBUG) {
				System.out.println("RCLRegistryNotifyThread end...");
			}
		}
	}

	
	/**
	 * @author marco
	 *
	 */
	private class RCLRegistryIsAliveThread extends Thread {

		/**
		 * 
		 */
		public RCLRegistryIsAliveThread() {
			super();
		}
		
		public void run () {
			
			if (RCLConstant.DEBUG) System.out.println("RCLRegistryIsAliveThread start...\n");
						
			Set<String> set = userAgentMap.keySet(); //no concurrentModificationExeption
			Iterator<String> iterator = null;
			RCLUserAgentRemoteInterface ua = null;
			String nick = null;
			
			while ( true ) {
				
				if (RCLConstant.DEBUG){
					
					System.out.println("\n------------------------------------------");
					
					System.out.println("RCLRegistryIsAliveThread: inizio controllo utenti online\n");
				}
				
				iterator = set.iterator();
				
				while( iterator.hasNext() ){
					
					nick = iterator.next();
					
					ua = userAgentMap.get( nick );
					
					try {
						
						ua.isAlive();
						
					} catch ( RemoteException e ) {
						
						if (RCLConstant.DEBUG) System.out.println("RCLRegistryIsAliveThread: UA di " + nick +" non risponde\n -> notifico passaggio offline");
						
						notifyThreadExecutor.execute( new RCLRegistryNotifyThread( nick, RCLStatus.OFFLINE ) );
						
						iterator.remove();
					}
				}
				
				if (RCLConstant.DEBUG){
					
					System.out.println("RCLRegistryIsAliveThread: fine controllo utenti online");
					
					System.out.println("------------------------------------------");
				}
				
				try {
					
					sleep( sleepTime );
					
				} catch ( InterruptedException e ) {
				}
			}
			
		}


	}
}

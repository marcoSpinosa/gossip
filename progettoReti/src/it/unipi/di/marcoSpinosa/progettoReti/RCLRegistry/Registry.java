/**
 *  Package per il progetto finale del Laboratorio di programmazione di rete
 */
package it.unipi.di.marcoSpinosa.progettoReti.RCLRegistry;

import it.unipi.di.marcoSpinosa.progettoReti.RCLConstant;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.regex.PatternSyntaxException;



/**
 * @author marco
 *
 */
public class Registry {
	
	/**
	 * @param args
	 */
	public static void main(String[ ] args) {
		
		try { 
				
				System.setProperty("java.rmi.server.hostname", RCLConstant.REGISTRY_IP);
			
				java.rmi.registry.Registry registry = LocateRegistry.createRegistry(RCLConstant.REGISTRY_PORT);
				/* Collega un nome al server nella registry */
				
				RCLRegistry server = new RCLRegistry();
				
				registry.rebind(RCLConstant.REGISTRY_SERVICE_NAME, server);
				
				if (RCLConstant.DEBUG){
					
					System.out.println(RCLConstant.REGISTRY_SERVICE_NAME + " Ready");
					
					BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
					
					String array[] = null;
					String in = null, command= null;
				
					while ( true ) {
							
							in = input.readLine().toLowerCase();
							
							try{		
									
								array = in.split(" ");																														
									
							} catch (PatternSyntaxException ex ){
								//Regex non valida ...Per come è strutturato il programma non dovrebbe essere mai lanciata
								System.out.println("ATTENZIONE!:\nPattern di riconoscimento comando non valido");
								registry.unbind( RCLConstant.REGISTRY_SERVICE_NAME );
								UnicastRemoteObject.unexportObject( server, true );
								System.exit(-1);
							}
							
							if (array.length != 1){
								
								System.out.println("ATTENZIONE!:\nTroppi parametri passati");
								System.out.println("Usa " + RCLConstant.HELP_COMMAND + " per visualizzare le istruzioni\n");
								continue;
							}
							
							command = array[0];
							
							switch( command ) {
							
								case RCLConstant.PROXY_INFO_COMMAND:
									server.infoProxy();
									break;
								
								case RCLConstant.NICK_INFO_COMMAND:
									server.getInfoByNickForRegistry( input.readLine().toLowerCase() );
									break;
									
								case RCLConstant.SLEEP_TIME_COMMAND:
									server.setSleepTimeForIsAliveThread( Integer.parseInt( input.readLine().toLowerCase() ) );
									break;
								
								case RCLConstant.HELP_COMMAND:
									RCLConstant.printRegistryHelp();
									break;
									
								default:
									System.out.println("Comando non valido\nUsa " + RCLConstant.HELP_COMMAND + " per visualizzare le istruzioni\n");
									break;				
							}
					}
				}
				
				else {
					
					System.out.println(RCLConstant.REGISTRY_SERVICE_NAME + " Ready");
				}
				
				
		} catch (Exception e) { 
				e.printStackTrace();
				System.exit(-1); 
		}
	}

}

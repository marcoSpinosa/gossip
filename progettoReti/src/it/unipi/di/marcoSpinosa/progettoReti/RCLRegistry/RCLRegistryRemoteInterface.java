/**
 * Package contenenti le classi per la realizzazione del componente Registry del servizio GOSSIP
 */
package it.unipi.di.marcoSpinosa.progettoReti.RCLRegistry;

import it.unipi.di.marcoSpinosa.progettoReti.RCLAddress;
import it.unipi.di.marcoSpinosa.progettoReti.RCLConstant;
import it.unipi.di.marcoSpinosa.progettoReti.RCLUserDataStruct;
import it.unipi.di.marcoSpinosa.progettoReti.RCLUserAgent.RCLUserAgentRemoteInterface;

import java.rmi.*;

/**
 * Interfaccia remota utilizzata dallo UA per creare uno stub locale del server
 * 
 * @author Marco Spinosa 477385
 */
public interface RCLRegistryRemoteInterface extends Remote {
	
	/**
	 * Funzione che registra un utente al servizio se il nickname scelto non è già presente
	 * 
	 * @param nickname dell'utente da registrare
	 * 
	 * @return 
	 * 			{@link RCLConstant#USER_ALREADY_REGISTERED USER_ALREADY_REGISTERED} 
	 * 			se l'utente chiede di registrarsi con un nickname già presente
	 * 		
	 * 		<p>	{@link RCLConstant#NO_PROXY NO_PROXY}
	 * 			se al momento della registrazione non è presente nessun proxy da assegnare al nuovo utente
	 * 		
	 * 		<p>	{@link RCLConstant#REGISTRATION_OK REGISTRATION_OK}
	 * 			se il nuovo utente è stato correttamente registrato con il suo nickname
	 * 
	 * @throws RemoteException Se non è possibile contattare il server
	 */
	public String register ( String nickname ) throws RemoteException;
	
	
	/**
	 * Funzione che connette nickname al servizio Gossip
	 * 
	 * @param nickname dell'utente da loggare
	 * @param userAgent da cui si è loggato l'utente
	 * 
	 * @return 
	 * 			{@link RCLConstant#USER_LOGGED_FROM_DIFFERENT_UA USER_LOGGED_FROM_DIFFERENT_UA}
	 * 			se l'utente è già connsesso da un altro userAgent
	 * 		
	 * 		<p>	{@link RCLConstant#UNREGISTERED_USER UNREGISTERED_USER} se l'utente non è registrato
	 * 		
	 * 		<p>	{@link RCLUserDataStruct#toJSON Stringa contentente le informazioni dell'utente} 
	 * 			se l'utente è stato connesso al servizio
	 * 
	 *@throws RemoteException Se non è possibile contattare il server
	 */
	public String login ( String nickname, RCLUserAgentRemoteInterface userAgent ) throws RemoteException;
	
	
	/**
	 * Funzione che disconnette nickname dal servizio Gossip
	 * 
	 * @param nickname dell'utente da disconnettere
	 * 
	 * @return {@link RCLConstant#LOGOUT_OK LOGOUT_OK} se l'utente è stato disconnesso dal servizio
	 * 
	 *@throws RemoteException Se non è possibile contattare il server
	 */
	public String logout ( String nickname ) throws RemoteException;
	
	
	/**
	 * Funzione che permette a nickname di accettare la richiesta di amicizia di allowedUser
	 * 
	 * @param nickname dell'utente che accetta la richiesta di amicizia
	 * @param allowedUser nickname dell'utente che ha inviato la richiesta di amicizia
	 * 
	 * @return {@link RCLConstant#ALLOW_OK ALLOW_OK}
	 * 
	 * @throws RemoteException Se non è possibile contattare il server
	 */
	public String allow ( String nickname, String allowedUser ) throws RemoteException;
	
	
	/**
	 * Funzione che permette a nickname di sbloccare allowedUser
	 * 
	 * @param nickname dell'utente che effettua l'operazione
	 * @param allowedUser nickname dell'utente da sbloccare
	 * 
	 * @return {@link RCLConstant#REALLOW_OK REALLOW_OK}
	 * 
	 * @throws RemoteException Se non è possibile contattare il server
	 */
	public String reallow ( String nickname, String allowedUser ) throws RemoteException;
	
	
	/**
	 * Funzione che permette a nickname di bloccare disallowUser
	 * 
	 * @param nickname dell'utente che effettua l'operazione
	 * @param disallowUser nickname dell'utente da bloccare
	 * 
	 * @return {@link RCLConstant#DISALLOW_OK DISALLOW_OK}
	 * 
	 * @throws RemoteException Se non è possibile contattare il server
	 */
	public String disallow ( String nickname, String disallowUser ) throws RemoteException;
	
	
	/**
	 * Funzione che permette a nickname di inviare una richiesta di amicizia a newFriend
	 * 
	 * @param nickname dell'utente che effettua l'operazione
	 * @param newFriend nickname dell'utente a cui inviare la richiesta
	 * 
	 * @return 
	 * 			{@link RCLConstant#UNREGISTERED_USER UNREGISTERED_USER}
	 * 			se newFriend non è registrato al servizio
	 * 
	 * 		<p> {@link RCLConstant#FRIEND_OK FRIEND_OK}
	 * 			Se la richiesta è stata inviata con successo
	 * 
	 * @throws RemoteException Se non è possibile contattare il server
	 */
	public String friend ( String nickname, String newFriend ) throws RemoteException;
	
	
	/**
	 * Funzione che permette a nickname di rimuovere dagli amici exFriend
	 * 
	 * @param nickname dell'utente che effettua l'operazione
	 * @param exFriend nickname dell'utente da eliminare dagli amici
	 * 
	 * @return {@link RCLConstant#UNFRIEND_OK UNFRIEND_OK}
	 * 
	 * @throws RemoteException Se non è possibile contattare il server
	 */
	public String unfriend ( String nickname, String exFriend ) throws RemoteException;
	
	
	/**
	 * Funzione che ristituisce l'indirizzo dello userAgent su cui è loggato
	 * l'utente richiesto se questo è online, o quello del proxy se l'utente è offline.<p>
	 * In entrambi i casi l'indirizzo è un {@link RCLAddress RCLAddress} in formato JSON
	 * 
	 * @param nickname di cui si vuole sapere l'indirizzo
	 * 
	 * @return Stringa che rappresenta l'indirizzo di nikcname.
	 * 
	 * @throws RemoteException Se non è possibile contattare il server
	 */
	public String getAddressByNickString ( String nickname ) throws RemoteException;
}
